package com.tintok

import android.content.Context
import com.auth0.android.jwt.JWT
import com.google.firebase.messaging.FirebaseMessaging
import java.util.*


private const val PREF_NAME = "com.tintok.SessionManager"
private const val TOKEN_NAME = "com.tintok.SessionManager.token"
private const val USER_ID = "com.tintok.SessionManager.userId"
private const val EXPIRY_TIME = "com.tintok.SessionManager.expiry"

/**
 * Set a new session token which will be saved in app storage
 */
fun Context.setSessionToken(token: String) {
    val relevantToken = token.split(" ")[1]
    val jwt = JWT(relevantToken)

    val editor = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
    editor.putString(TOKEN_NAME, token)
    editor.apply()

    setTokenExpiry(jwt.expiresAt!!)
    setUserId(jwt.getClaim("id").asString()!!)
}

/**
 * Get the user id from the current logged in user.
 */
fun Context.getUserId(): String = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(USER_ID, "")!!

/**
 * Get a session token from app storage
 */
fun Context.getSessionToken(): String =
    getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(TOKEN_NAME, "")!!

/**
 * Check if the token is valid
 */
fun Context.hasValidSession(): Boolean {
    val cal = Calendar.getInstance()
    cal.time = Date()
    cal.add(Calendar.HOUR, -48)

    return getTokenExpiry().after(cal.time)
}

/**
 * Check if the token is refreshable
 */
fun Context.isTokenRefreshable(): Boolean = getSessionToken() != ""

/**
 * Get the Expiry of a token
 */
fun Context.getTokenExpiry(): Date =
    Date(getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getLong(EXPIRY_TIME, 0L))

/**
 * Set the user id from the current logged in user.
 */
private fun Context.setTokenExpiry(time: Date) {
    val editor = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
    editor.putLong(EXPIRY_TIME, time.time)
    editor.apply()
}

/**
 * Set the user id from the current logged in user.
 */
private fun Context.setUserId(id: String) {
    val editor = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
    editor.putString(USER_ID, id)
    editor.apply()
}

fun getFcmToken(successListener: ((String) -> Unit)) =
    FirebaseMessaging.getInstance().token.addOnSuccessListener(successListener)


fun Context.clearUserData() {
    val editor = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
    editor.clear()
    editor.apply()
}

fun deleteFcmToken(successListener: ((Void) -> Unit)) =
    FirebaseMessaging.getInstance().deleteToken().addOnSuccessListener(successListener)
