package com.tintok

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tintok.api.FcmService
import com.tintok.api.model.fcm.FcmToken
import com.tintok.api.model.fcm.FirebaseMessage
import com.tintok.api.model.fcm.MessageType
import com.tintok.ui.chat.CHAT_ID
import com.tintok.ui.chat.CHAT_NAME
import com.tintok.ui.chat.ChatActivity
import com.tintok.util.*

// A Service not registered: com.google.android.gms.measurement.internal is thrown by the service. No worries, see at
// https://github.com/firebase/firebase-android-sdk/issues/1662#issuecomment-756917362

/**
 * NotificationService represents the Firebase Messaging Service Implementation to handle Firebase Tokens for notifications
 */
class NotificationService : FirebaseMessagingService() {
    private lateinit var broadcaster: LocalBroadcastManager
    private lateinit var fcmService: FcmService
    private lateinit var notificationManager: NotificationManager

    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)
        fcmService = FcmService(this)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        if (this.hasValidSession()) {
            fcmService.api.addFcmKeyToAccount(FcmToken(newToken)).enqueue(
                fcmService.handleApiResponse(
                    mustContainSuccessCode = 204
                )
            )
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        try {
            Log.d(this.javaClass.name, "Firebase message ${remoteMessage.messageId} received.")
            val message = FirebaseMessage(remoteMessage.data)
            sendNotification(message)
            if (message.isChatMessage()) {
                Log.d(this.javaClass.name, "Send new message broadcast.")
                val intent = Intent(MESSAGE_LISTENER_NAME)
                intent.putExtra(MESSAGE_INTENT_EXTRA_NAME, message)
                broadcaster.sendBroadcast(intent)
            }
        } catch (exception: Exception) {
            Log.e(this.javaClass.name, exception.message.toString())
        }
    }

    private fun FirebaseMessage.isChatMessage() = MessageType.valueOf(type) == MessageType.CHAT

    /**
     * Send a notification to user build from a firebase message
     */
    private fun sendNotification(message: FirebaseMessage) {
        val notifications = buildNotifications(message)

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelName =
                if (message.isChatMessage()) getString(R.string.notification_channel_chats)
                else getString(R.string.notification_channel_matches)
            val channel = NotificationChannel(
                notifications.first.channelId,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.vibrationPattern = longArrayOf(0L, 250L, 250L, 250L)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.apply {
            notify(message.baseNotificationId, notifications.first)
            notify(message.summaryNotificationId, notifications.second)
        }
    }

    /**
     * Build a notification depending on it's type
     */
    private fun buildNotifications(message: FirebaseMessage): Pair<Notification, Notification> {
        val channelId =
            if (message.isChatMessage()) NOTIFICATION_CHANNEL_ID_CHATS
            else NOTIFICATION_CHANNEL_ID_MATCHES

        val baseNotification =
            NotificationCompat.Builder(this, channelId)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(buildIntentToLaunchChat((message)))
                .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_SUMMARY)
                .addProfilePicture(this, message.id)

        val summaryNotification =
            NotificationCompat.Builder(this, channelId)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setGroupSummary(true)
                .setAutoCancel(true)

        when (MessageType.valueOf(message.type)) {
            MessageType.CHAT -> {
                baseNotification
                    .setContentTitle(getString(R.string.notification_title_chat, message.name))
                    .setContentText(message.message)
                    .setSmallIcon(R.drawable.ic_chat_black_24dp)
                    .setGroup(NOTIFICATION_GROUP_CHATS)

                summaryNotification
                    .setSmallIcon(R.drawable.ic_chat_black_24dp)
                    .setGroup(NOTIFICATION_GROUP_CHATS)
            }
            MessageType.MATCH -> {
                baseNotification
                    .setContentTitle(getString(R.string.notification_title_match, message.name))
                    .setSmallIcon(R.drawable.ic_profile_black_24dp)
                    .setGroup(NOTIFICATION_GROUP_MATCHES)

                summaryNotification
                    .setSmallIcon(R.drawable.ic_profile_black_24dp)
                    .setGroup(NOTIFICATION_GROUP_MATCHES)
            }
        }

        return Pair(baseNotification.build(), summaryNotification.build())
    }

    /**
     * Synchronously load user's profile picture and add it to notification
     */
    private fun NotificationCompat.Builder.addProfilePicture(
        context: Context,
        userId: String
    ): NotificationCompat.Builder {
        try {
            val profilePictureBitmap =
                GlideApp.with(context).asBitmap().load(getUserImageUrl(userId)).submit().get()
            this.setLargeIcon(profilePictureBitmap)
        } catch (e: Exception) {
            Log.d(context.javaClass.name, e.message.toString())
        }
        return this
    }

    /**
     * Build the intent to launch ChatActivity when notification is clicked
     */
    private fun buildIntentToLaunchChat(message: FirebaseMessage): PendingIntent {
        val intent = Intent(this, ChatActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            .putExtra(CHAT_ID, message.id)
            .putExtra(CHAT_NAME, message.name)

        return TaskStackBuilder.create(this).run {
            addNextIntentWithParentStack(intent)
            getPendingIntent(message.baseNotificationId, PendingIntent.FLAG_UPDATE_CURRENT)
        }
    }
}