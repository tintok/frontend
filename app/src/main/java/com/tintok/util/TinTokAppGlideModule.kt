package com.tintok.util

import android.content.Context
import android.util.Log
import androidx.annotation.Keep
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideExtension
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.annotation.GlideOption
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.BaseRequestOptions
import com.tintok.R
import com.tintok.api.BASE_URL
import com.tintok.api.getDefaultClient
import java.io.InputStream

fun getPostImageUrl(postId: String): GlideUrl = GlideUrl("${BASE_URL}posts/$postId/image")

fun getUserImageUrl(userId: String): GlideUrl = GlideUrl("${BASE_URL}users/$userId/profile-picture")

/**
 * TinTokAppGlideModule is used to load and cache images for profile avatars and posts.
 */
@Keep
@GlideModule
class TinTokAppGlideModule : AppGlideModule() {

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        val client = getDefaultClient(context)
        registry.replace(
            GlideUrl::class.java,
            InputStream::class.java,
            OkHttpUrlLoader.Factory(client)
        )
    }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        builder.setLogLevel(Log.ERROR)
        super.applyOptions(context, builder)
    }

}

/**
 * TinTokGlideExtensions extends the default Glide client and adds functions for caching to it.
 */
@GlideExtension
class TinTokGlideExtensions private constructor() {

    companion object {

        /**
         * Default Options for loading pictures of image posts
         */
        @JvmStatic
        @GlideOption
        fun applyDefaultImagePostsOptions(
            options: BaseRequestOptions<*>,
            context: Context
        ): BaseRequestOptions<*> {
            val progressBar = CircularProgressDrawable(context).apply {
                strokeWidth = 5f
                centerRadius = 90f
            }
            progressBar.start()

            return options
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .placeholder(progressBar)
                .error(R.drawable.ic_baseline_image_not_found_24)

        }

        /**
         * Default Options for loading profile pictures
         */
        @JvmStatic
        @GlideOption
        fun applyDefaultProfileOptions(options: BaseRequestOptions<*>): BaseRequestOptions<*> =
            options
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.ic_profile_black_24dp)
                .centerCrop()

        @JvmStatic
        @GlideOption
        fun enableGlideCaching(options: BaseRequestOptions<*>): BaseRequestOptions<*> = options
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .skipMemoryCache(false)

    }

}
