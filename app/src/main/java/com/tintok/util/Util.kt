package com.tintok.util

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.View.TEXT_ALIGNMENT_GRAVITY
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.forEach
import com.abedelazizshe.lightcompressorlibrary.CompressionListener
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor
import com.abedelazizshe.lightcompressorlibrary.VideoQuality
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSource
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.tintok.R
import com.tintok.api.BASE_URL
import com.tintok.api.getDefaultClient
import com.tintok.getUserId
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import permissions.dispatcher.PermissionRequest
import java.io.File
import java.util.*


/**
 * Hide the user's keyboard
 */
fun hideKeyboard(context: Context, view: View) =
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(view.windowToken, 0)

/**
 * Trim the text of a EditText and return null if the input is blank
 */
fun EditText.nullableText(): String? = text.toString().trim().ifBlank { null }

/**
 * Replace all occurrences of 3 or more white space lines with 2 line breaks
 */
fun EditText.lessWhiteSpacedText(): String? = this.nullableText()?.replace(
    Regex("(\\s*[\n\r]){3,}"),
    "\n\n"
)

/**
 * Replace any whitespace with a single space
 */
fun String.onlySingleSpaces() = this.replace(
    Regex("\\s+"),
    " "
)

/**
 * Set trimmed text to a TextView and optionally hide/show view or change TextView gravity based on text
 */
fun TextView.updateWithText(
    text: String? = this.text.toString(),
    visibilityOnBlankText: Int? = null,
    textLengthGravityThreshold: Int? = null,
) {
    val trimmedText = text?.trim() ?: ""
    if (textLengthGravityThreshold != null) {
        this.textAlignment = TEXT_ALIGNMENT_GRAVITY
        this.gravity = when {
            trimmedText.length > textLengthGravityThreshold -> Gravity.START or Gravity.CENTER_VERTICAL
            else -> Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL
        }
    }
    this.text = trimmedText
    if (visibilityOnBlankText != null) {
        this.visibility = when {
            trimmedText.isBlank() -> visibilityOnBlankText
            else -> View.VISIBLE
        }
    }
}

/**
 * Set an callback which will be executed,
 * when the user presses the done button on the keyboard
 */
fun setKeyboardDoneAction(textView: TextView, callback: () -> Unit) =
    textView.setOnEditorActionListener(
        TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                callback()
                return@OnEditorActionListener true
            } else false
        }
    )

fun showInternetError(context: Context) = showToast(context, R.string.internet_error)

fun showInternalError(context: Context) = showToast(context, R.string.internal_error)

fun showToast(context: Context, messageId: Int, long: Boolean = false) =
    Toast.makeText(context, messageId, if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT)
        .show()

fun showToast(context: Context, message: String, long: Boolean = false) =
    Toast.makeText(context, message, if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT)
        .show()

/**
 * Get a MultipartBody.Part for uploading an image
 */
fun getMediaUploadBody(
    file: File,
    formDataName: String,
    mediaGroupType: String = "image"
): MultipartBody.Part {
    val mediaType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
        MimeTypeMap.getFileExtensionFromUrl(
            file.toUri().toString()
        ).toLowerCase(Locale.ROOT)
    )
    Log.d("MEDIA_UPLOAD_FILE_WITH_MIME_TYPE", mediaType.toString())

    val requestFile = RequestBody.create(
        MediaType.parse(mediaType ?: mediaGroupType + "/" + file.extension),
        file
    )

    return MultipartBody.Part.createFormData(formDataName, file.name, requestFile)
}

val File.size get() = if (!exists()) 0.0 else length().toDouble()
val File.sizeInKb get() = size / 1024
val File.sizeInMb get() = sizeInKb / 1024

/**
 * Compress a given video when it's file size is above the threshold
 * and save result to app cache as .tmp file with an unique name
 */
fun compressVideo(
    context: Context,
    originalFile: File,
    successCallback: (outputFile: File) -> Unit,
    errorCallback: () -> Unit = {},
) {
    if (originalFile.sizeInMb >= VIDEO_COMPRESSION_THRESHOLD_MB) {
        val outputDir: File = context.cacheDir
        val outputFile = File.createTempFile("video", "." + originalFile.extension, outputDir)
        GlobalScope.launch {
            VideoCompressor.start(
                originalFile.path,
                outputFile.path,
                object : CompressionListener {
                    override fun onProgress(percent: Float) {}

                    override fun onStart() {}

                    override fun onSuccess() {
                        if (outputFile.sizeInMb >= FILE_UPLOAD_MAX_MB) {
                            showToast(context, R.string.error_video_too_large)
                            errorCallback()
                        } else {
                            successCallback(outputFile)
                        }
                    }

                    override fun onFailure(failureMessage: String) {
                        Log.e("VIDEO_COMPRESSION_ERROR", failureMessage)
                        showInternalError(context)
                        errorCallback()
                    }

                    override fun onCancelled() {}
                },
                VideoQuality.HIGH,
                false,
            )
        }
    } else {
        successCallback(originalFile)
    }
}

/**
 * Perform a function on every descendant of a view group
 */
fun ViewGroup.deepForEach(function: View.() -> Unit) {
    this.forEach { child ->
        child.function()
        if (child is ViewGroup) {
            child.deepForEach(function)
        }
    }
}

/**
 * Get a color from theme
 */
fun Context.themeColor(@AttrRes attrRes: Int): Int {
    val typedValue = TypedValue()
    theme.resolveAttribute(attrRes, typedValue, true)
    return typedValue.data
}


/**
 * Creates a SimpleExoPlayer.Builder with okhttp3 support and set headers
 * using our default client
 */
fun getDefaultPlayerBuilder(context: Context): SimpleExoPlayer.Builder {
    val factory = DefaultMediaSourceFactory(OkHttpDataSource.Factory(getDefaultClient(context)))
    return SimpleExoPlayer.Builder(context).setMediaSourceFactory(factory)
}

/**
 * Get the uri of the video content in a video post
 */
fun getVideoUri(id: String): Uri = Uri.parse("${BASE_URL}posts/${id}/video")


/**
 * Get the users avatar from backend and set a placeholder image, if an error occurs
 */
fun Context.loadUserAvatar(viewAvatar: ImageView) {
    GlideApp.with(this).asDrawable().load(getUserImageUrl(getUserId()))
        .applyDefaultProfileOptions()
        .into(viewAvatar)
}

fun Context.showPermissionRationale(
    request: PermissionRequest,
    titleId: Int,
    messageId: Int,
    dismissCallback: () -> Unit = {}
) {
    var onDismissCallback = dismissCallback
    MaterialAlertDialogBuilder(this)
        .setTitle(titleId)
        .setMessage(messageId)
        .setNegativeButton(R.string.cancel) { _, _ -> request.cancel() }
        .setPositiveButton(R.string.ok) { _, _ ->
            onDismissCallback = {}
            request.proceed()
        }
        .setOnDismissListener { onDismissCallback() }
        .show()
}

fun Context.showPermissionDeniedWarning(messageId: Int) = showToast(this, messageId, true)

fun Context.showPermissionNeverAskAgainWarning(titleId: Int, messageId: Int) {
    MaterialAlertDialogBuilder(this)
        .setTitle(titleId)
        .setMessage(messageId)
        .setNegativeButton(R.string.cancel) { _, _ -> }
        .setPositiveButton(R.string.open_settings) { _, _ ->
            openAndroidAppSettings(this)
        }
        .show()
}

fun openAndroidAppSettings(context: Context) = ContextCompat.startActivity(
    context,
    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
        data = Uri.fromParts("package", context.packageName, null)
    },
    null
)

fun userIdToInt(userId: String) = userId.toCharArray()
    .fold(0) { sum, char -> sum + char.toInt() }

/**
 * Cancel a specific notification
 * Also cancel it's summaryNotification if the summary is empty
 */
fun NotificationManager.cancelGroupNotification(
    notificationGroupKey: String,
    baseNotificationId: Int,
    summaryNotificationId: Int
) {
    this.cancel(baseNotificationId)
    val shouldCancelSummaryNotification = this.activeNotifications.none {
        it.notification.group == notificationGroupKey
                && it.id != baseNotificationId
                && it.id != summaryNotificationId
    }
    if (shouldCancelSummaryNotification)
        this.cancel(summaryNotificationId)
}
