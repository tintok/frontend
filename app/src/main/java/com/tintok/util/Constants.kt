package com.tintok.util

const val REQUEST_CODE_VIDEO_PICKER = 1
const val FILE_UPLOAD_MAX_MB = 55
const val VIDEO_COMPRESSION_THRESHOLD_MB = 5
const val VIDEO_SELECTION_MAX_MB = 300

const val MESSAGE_LISTENER_NAME = "com.tintok.MESSAGE"
const val MESSAGE_INTENT_EXTRA_NAME = "message"

const val NOTIFICATION_CHANNEL_ID_CHATS = "com.tintok.notification.CHATS"
const val NOTIFICATION_CHANNEL_ID_MATCHES = "com.tintok.notification.MATCHES"
const val NOTIFICATION_GROUP_CHATS = NOTIFICATION_CHANNEL_ID_CHATS
const val NOTIFICATION_GROUP_MATCHES = NOTIFICATION_CHANNEL_ID_MATCHES


val supportedImageMimeTypes = arrayOf(
    "image/bmp",
    "image/gif",
    "image/jpeg",
    "image/jpg",
    "image/png",
    "image/webp",
)

val supportedImageExtensions = arrayOf(
    "bmp",
    "gif",
    "jpeg",
    "jpg",
    "png",
    "webp",
)

val supportedVideoExtensions = arrayOf(
    "mp4",
    "webm"
)