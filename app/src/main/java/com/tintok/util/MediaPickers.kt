package com.tintok.util

import android.app.Activity
import androidx.fragment.app.Fragment
import com.deepakkumardk.videopickerlib.EasyVideoPicker
import com.deepakkumardk.videopickerlib.model.SelectionMode
import com.deepakkumardk.videopickerlib.model.VideoPickerItem
import com.github.dhaval2404.imagepicker.ImagePicker
import com.tintok.R


/**
 * Get an ImagePicker with the default config for an Activity
 */
fun getImagePicker(activity: Activity) =
    setDefaultImagePickerConfig(ImagePicker.with(activity))

/**
 * Get an ImagePicker with the default config for a Fragment
 */
fun getImagePicker(fragment: Fragment) =
    setDefaultImagePickerConfig(ImagePicker.with(fragment))

/**
 * Set the default config for an ImagePicker
 */
private fun setDefaultImagePickerConfig(imagePicker: ImagePicker.Builder): ImagePicker.Builder {
    return imagePicker
        .crop() // Enable cropping image
        .compress(1024)    // Final image size will be less than 1 MB
        .maxResultSize(1080, 1080) // Final image resolution will be less than 1080 x 1080
        .galleryMimeTypes(supportedImageMimeTypes)
}

/**
 * Start a VideoPicker with the default config for an Fragment
 */
fun startVideoPicker(fragment: Fragment) {
    val videoPickerConfig = VideoPickerItem().apply {
        showDuration = true
        themeResId = R.style.Theme_TinTok
        selectionMode = SelectionMode.Single
    }
    EasyVideoPicker().startPickerForResult(fragment, videoPickerConfig, REQUEST_CODE_VIDEO_PICKER)
}