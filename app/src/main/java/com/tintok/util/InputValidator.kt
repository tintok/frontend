package com.tintok.util

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator

/**
 * InputValidator validates the field in Forms for correct values (e.g. chat messages are only 512 letters or less).
 */
class InputValidator(controller: Any, private val context: Context) {

    private val validator = Validator(controller)
    private var validationSuccessCallback: () -> Unit = {}
    private var validationErrorCallback: () -> Unit = {}
    private var pauseValidation = false

    /**
     * Set the validation listener and remove any errors from view if it was validated
     */
    init {
        setValidationListener()

        validator.setViewValidatedAction { view ->
            if (view is TextView) {
                view.error = null
            }
            val viewParentParent = view?.parent?.parent
            if (viewParentParent is TextInputLayout) {
                viewParentParent.error = null
            }
        }
    }

    /**
     * Set the validation listener to call callbacks
     * and show errors on unsuccessful validation
     */
    private fun setValidationListener() {
        validator.setValidationListener(object : Validator.ValidationListener {
            override fun onValidationSucceeded() {
                validationSuccessCallback()
            }

            override fun onValidationFailed(errors: MutableList<ValidationError>) {
                if (!pauseValidation) {
                    errors.forEach { error ->
                        val view = error.view
                        val viewParentParent = view.parent.parent
                        val message = error.getCollatedErrorMessage(context)

                        when {
                            viewParentParent is TextInputLayout -> viewParentParent.error = message
                            view is TextInputEditText -> view.error = message
                            else -> showToast(context, message)
                        }
                    }
                    validationErrorCallback()
                }
            }
        })
    }

    /**
     * Temporary pause validation until the supplied callback is finished
     */
    fun pauseValidationForCallback(callback: () -> Unit) {
        pauseValidation = true
        callback()
        pauseValidation = false
    }

    /**
     * Set callbacks for successful and unsuccessful validation
     */
    fun setValidationCallbacks(success: () -> Unit, error: () -> Unit): InputValidator {
        validationSuccessCallback = success
        validationErrorCallback = error
        return this
    }

    /**
     * Register the inputs which will be validated.
     * All inputs need to have the @Order annotation.
     */
    fun registerInputs(
        validateBeforeInputs: List<EditText> = listOf(),
        validateTillInputs: List<EditText> = listOf()
    ): InputValidator {
        validateBeforeInputs.forEach { addInputListeners(it) }
        validateTillInputs.forEach { addInputListeners(it, false) }
        return this
    }

    /**
     * Add listeners to run validation when the input's text or focus changed
     */
    private fun addInputListeners(input: EditText, validateBefore: Boolean = true) {
        input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (validateBefore) validator.validateBefore(input)
                else validator.validateTill(input)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        input.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                validator.validateBefore(input)
            }
        }
    }
}