package com.tintok.ui.profile

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.VectorDrawable
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.mobsandgeeks.saripaar.annotation.Length
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Order
import com.tintok.R
import com.tintok.api.UsersService
import com.tintok.api.model.users.PutUserMe
import com.tintok.ui.ActivityUsingPermissions
import com.tintok.ui.MediaCallback
import com.tintok.ui.mediaActionWithPermissionCheck
import com.tintok.util.*
import okhttp3.ResponseBody
import java.io.File

/**
 * ProfileEditActivity is used by the user to update his own profile information.
 * ProfileEditActivity is started by clicking on the "Edit Profile" button in the ProfileFragment.
 */
class ProfileEditActivity : ActivityUsingPermissions() {

    private lateinit var viewAvatar: ImageView
    private lateinit var usersService: UsersService
    private lateinit var btnSave: Button
    private lateinit var validator: InputValidator

    @NotEmpty(messageResId = R.string.error_first_name, trim = true)
    @Length(max = 64, trim = true, messageResId = R.string.input_error_max_length_64)
    @Order(0)
    private lateinit var inputGivenName: TextInputEditText

    @NotEmpty(messageResId = R.string.error_last_name, trim = true)
    @Length(max = 64, trim = true, messageResId = R.string.input_error_max_length_64)
    @Order(1)
    private lateinit var inputFamilyName: TextInputEditText

    @Length(max = 512, messageResId = R.string.input_error_max_length_512)
    @Order(2)
    private lateinit var inputBio: TextInputEditText

    @Length(max = 64, messageResId = R.string.input_error_max_length_64)
    @Order(3)
    private lateinit var inputLocation: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        usersService = UsersService(this)
        setContentView(R.layout.activity_profile_edit)

        viewAvatar = findViewById(R.id.post_view_avatar)
        inputGivenName = findViewById(R.id.profile_input_given_name)
        inputFamilyName = findViewById(R.id.profile_input_family_name)
        inputBio = findViewById(R.id.post_input_text)
        inputLocation = findViewById(R.id.profile_input_location)
        btnSave = findViewById(R.id.profile_btn_save)

        setupValidationAndButtons()

        LoadUserMeService(this).loadUser({
            inputGivenName.setText(it.givenName)
            inputFamilyName.setText(it.familyName)
            inputBio.setText(it.biography)
            inputLocation.setText(it.location)
        })
        loadUserAvatar(viewAvatar)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == ImagePicker.REQUEST_CODE) {
            try {
                uploadAvatar(ImagePicker.getFile(data)!!)
            } catch (error: NullPointerException) {
                Log.e("ProfileEditActivityResult", "Could not process activity result: ${error.message}")
                showToast(this, R.string.error_media_selection)
            }
        }
    }

    /**
     * Setup input validation and button actions
     */
    private fun setupValidationAndButtons() {
        validator = InputValidator(this, this)
            .registerInputs(
                listOf(inputGivenName, inputFamilyName, inputBio),
                listOf(inputLocation)
            )
            .setValidationCallbacks(
                { btnSave.isEnabled = true },
                { btnSave.isEnabled = false },
            )

        setKeyboardDoneAction(inputLocation) { btnSave.performClick() }
        btnSave.setOnClickListener {
            if (btnSave.isEnabled) {
                hideKeyboard(this, it)
                saveProfile()
            }
        }

        viewAvatar.setOnClickListener {
            editAvatar()
        }
    }

    /**
     * Save the user's profile to backend
     */
    private fun saveProfile() {
        val givenName = inputGivenName.nullableText()
        val familyName = inputFamilyName.nullableText()

        if (givenName != null && familyName != null) {
            val putUserMe = PutUserMe(
                givenName,
                familyName,
                inputLocation.nullableText(),
                inputBio.lessWhiteSpacedText()
            )

            usersService.api.putMe(putUserMe).enqueue(
                usersService.handleApiResponse<ResponseBody>(
                    mustContainSuccessCode = 204,
                    successCallback = {
                        setResult(RESULT_OK)
                        finish()
                    })
            )
        } else {
            showToast(this, R.string.error_required_fields_missing)
        }
    }

    /**
     * Edit user's avatar
     */
    private fun editAvatar() {
        if (viewAvatar.drawable != null && viewAvatar.drawable !is VectorDrawable) {
            MaterialAlertDialogBuilder(this)
                .setItems(
                    arrayOf(
                        getString(R.string.new_profile_photo),
                        getString(R.string.remove_photo)
                    )
                ) { _, selectedOptionIndex: Int ->
                    when (selectedOptionIndex) {
                        0 -> pickImage()
                        1 -> deleteProfilePicture()
                    }
                }
                .show()
        } else {
            pickImage()
        }
    }

    private fun pickImage() =
        mediaActionWithPermissionCheck(MediaCallback {
            getImagePicker(this).start()
        })

    /**
     * Send delete request for user's avatar to backend
     */
    private fun deleteProfilePicture() {
        usersService.api.deleteOwnProfilePicture().enqueue(
            usersService.handleApiResponse<ResponseBody>(
                mustContainSuccessCode = 204,
                successCallback = {
                    setResult(RESULT_OK)
                    showToast(this, R.string.photo_removed)
                    loadUserAvatar(viewAvatar)
                })
        )
    }

    /**
     * Upload user's avatar to backend
     */
    private fun uploadAvatar(file: File) {
        val call = usersService.api.postOwnProfilePicture(
            getMediaUploadBody(file, "picture")
        )

        call.enqueue(
            usersService.handleApiResponse<ResponseBody>(
                mustContainSuccessCode = 204,
                successCallback = {
                    setResult(RESULT_OK)
                    loadUserAvatar(viewAvatar)
                })
        )
    }

}