package com.tintok.ui.profile

/**
 * Profile represents the data that is shown on top of the profile for each user.
 */
data class Profile(
    var name: String? = null,
    var bio: String? = null,
    var location: String? = null
)