package com.tintok.ui.profile

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.robertlevonyan.views.chip.Chip
import com.tintok.R
import com.tintok.api.PostsService
import com.tintok.api.model.posts.Post
import com.tintok.api.model.posts.PostType
import com.tintok.ui.FullscreenActivity
import com.tintok.ui.chat.ChatListFragment
import com.tintok.ui.settings.SettingsActivity
import com.tintok.util.*
import org.ocpsoft.prettytime.PrettyTime
import java.util.*
import kotlin.collections.ArrayList

/**
 * Adapter for the recyclerview in the [ProfileViewAdapter].
 * creates and binds [ProfileViewHolder], [VideoViewHolder], [TextViewHolder] and [ImageViewHolder] to display in the recyclerview
 * implements user interactions with the ViewHolders
 */
class ProfileViewAdapter(
    private val fragment: Fragment,
    private val userId: String,
    val profile: Profile,
    val postList: MutableList<Post> = ArrayList()
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), ListPreloader.PreloadModelProvider<GlideUrl> {
    private val service: PostsService = PostsService(fragment.requireContext())
    private var players: MutableList<Player?> = ArrayList()

    override fun getItemViewType(position: Int) =
        if (position == 0)
            R.id.profile_info
        else {
            when (postList[position - 1].type) {
                PostType.IMAGE -> R.id.profile_img_item
                PostType.VIDEO -> R.id.profile_vid_item
                PostType.TEXT -> R.id.profile_text_item
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View

        return when (viewType) {
            R.id.profile_info -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.profile_info, parent, false)
                ProfileViewHolder(view)
            }
            R.id.profile_text_item -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.profile_text_post, parent, false)
                TextViewHolder(view)
            }
            R.id.profile_img_item -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.profile_image_post, parent, false)
                ImageViewHolder(view)
            }
            R.id.profile_vid_item -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.profile_video_post, parent, false)
                VideoViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown Post Id: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PostViewHolder) {
            setupPostViewHolder(holder, postList[position - 1])
            setupPopupMenu(holder)
            when (holder) {
                is ImageViewHolder -> setupImageViewHolder(holder, postList[position - 1])
                is VideoViewHolder -> setupVideoViewHolder(holder, postList[position - 1])
            }
        } else if (holder is ProfileViewHolder) {
            setupProfileView(holder)
        }
    }

    /**
     * Assign corresponding values to the main ProfileViewHolder
     */
    private fun setupProfileView(holder: ProfileViewHolder) {
        holder.viewName.text = profile.name
        holder.viewBio.updateWithText(profile.bio, View.GONE, 60)
        holder.viewLocation.updateWithText(profile.location, View.GONE)
        loadUserIcon(holder.viewAvatar)
        holder.btnEdit.setOnClickListener {
            fragment.startActivityForResult(
                Intent(fragment.requireContext(), ProfileEditActivity::class.java),
                PROFILE_EDIT
            )
        }
        holder.btnSetting.setOnClickListener {
            startActivity(
                fragment.requireContext(),
                Intent(fragment.requireContext(), SettingsActivity::class.java),
                null
            )
        }
    }

    /**
     * Set up an OnClickListener to create a popup menu
     * with the options to edit/delete posts
     */
    private fun setupPopupMenu(holder: PostViewHolder) {
        holder.popupButton.setOnClickListener {
            val popupMenu = PopupMenu(fragment.requireContext(), holder.popupButton)
            popupMenu.inflate(R.menu.post_menu)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.delete_post -> {
                        deletePost(holder)
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }

    /**
     * sends a request to delete the selected post
     * and updates the view when the request succeeds
     */
    private fun deletePost(holder: PostViewHolder) {
        MaterialAlertDialogBuilder(fragment.requireContext())
            .setTitle(R.string.delete_post_question)
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .setPositiveButton(R.string.delete) { _, _ ->
                service.api.deletePost(postList[holder.adapterPosition - 1].id).enqueue(
                    service.handleApiResponse(
                        successCallback = {
                            postList.removeAt(holder.adapterPosition - 1)
                            notifyItemRemoved(holder.adapterPosition)
                        }
                    )
                )
            }
            .show()
    }

    /**
     * Loads the profile picture
     */
    private fun loadUserIcon(userIcon: ImageView) {
        GlideApp.with(fragment).asDrawable().load(getUserImageUrl(userId))
            .applyDefaultProfileOptions()
            .into(userIcon)
    }


    /**
     * Assign corresponding values to a basic ViewHolder Layout
     * Called by every PostViewHolder
     */
    private fun setupPostViewHolder(holder: PostViewHolder, post: Post) {
        holder.textView.text = post.text
        holder.username.text = profile.name
        holder.timestamp.text =
            PrettyTime().format(Date(post.created.toEpochSecond() * DateUtils.SECOND_IN_MILLIS))
        loadUserIcon(holder.userIcon)
        post.tags.forEach {
            val view =
                LayoutInflater.from(holder.tagsView.context)
                    .inflate(R.layout.tag_chip_feed, holder.tagsView, false)
            val chip = view.findViewById<Chip>(R.id.tag_chip)
            chip.setText(it)
            holder.tagsView.addView(chip)
        }
    }

    /**
     * Loads the image into the view
     */
    private fun setupImageViewHolder(holder: ImageViewHolder, post: Post) {
        holder.imgView.setOnClickListener(null)
        GlideApp.with(fragment).asDrawable().load(getPostImageUrl(post.id))
            .applyDefaultImagePostsOptions(fragment.requireContext())
            .centerCrop()
            .addListener(DefaultRequestListener(fragment.requireContext(), holder.imgView, post.id))
            .into(holder.imgView)

        // hide/show text view if post has text
        if (post.text == null)
            holder.textView.visibility = View.GONE
        else holder.textView.visibility = View.VISIBLE
    }

    /**
     * Load the video into the view
     */
    private fun setupVideoViewHolder(holder: VideoViewHolder, post: Post) {
        val player = getDefaultPlayerBuilder(fragment.requireContext()).build()
        players.add(player)
        holder.vidView.player = player

        player.setMediaItem(MediaItem.fromUri(getVideoUri(post.id)))
        player.prepare()

        // hide/show text view if post has text
        if (post.text == null)
            holder.textView.visibility = View.GONE
        else holder.textView.visibility = View.VISIBLE
    }

    override fun getItemCount() = postList.size + 1

    override fun getPreloadItems(position: Int): MutableList<GlideUrl> {
        val list: MutableList<GlideUrl> = ArrayList()
        if (position == 0)
            list.add(getUserImageUrl(userId))
        else if (position < itemCount) {
            list.add(getUserImageUrl(userId))
            if (postList[position - 1].type == PostType.IMAGE)
                list.add(getPostImageUrl(postList[position - 1].id))
        }
        return list
    }


    override fun getPreloadRequestBuilder(item: GlideUrl): RequestBuilder<*> =
        GlideApp.with(fragment).asDrawable().load(item)


    /**
     * pause all videos
     */
    fun pauseVideos() = players.forEach { it?.pause() }

    /**
     * release all video players
     */
    fun releasePlayers() {
        players.forEach { it?.release() }
        players = ArrayList()
    }

    /**
     * pause videos when the view is not visible anymore
     */
    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        Log.d("ProfileViewAdapter: ", "OnDetached")
        if (holder is VideoViewHolder) {
            holder.vidView.player?.pause()
        }
        super.onViewDetachedFromWindow(holder)
    }


    /**
     * release video players when the view gets recycled
     */
    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        if (holder is PostViewHolder)
            holder.tagsView.removeAllViews()
        if (holder is VideoViewHolder) {
            val player = holder.vidView.player
            players.remove(player)
            player?.release()
        }
        super.onViewRecycled(holder)
    }
}

/**
 * Represents a post in the UI
 */
sealed class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val popupButton: ImageButton = itemView.findViewById(R.id.post_popup_menu)
    val textView: TextView = itemView.findViewById(R.id.profile_post_text)
    val username: TextView = itemView.findViewById(R.id.username)
    val userIcon: ImageView = itemView.findViewById(R.id.user_icon)
    val timestamp: TextView = itemView.findViewById(R.id.time)
    val tagsView: LinearLayout = itemView.findViewById(R.id.profile_tags_view)
}

/**
 * Represents a post of type TEXT in the UI
 */
class TextViewHolder(itemView: View) : PostViewHolder(itemView)

/**
 * Represents a post of type IMAGE in the UI
 */
class ImageViewHolder(itemView: View) : PostViewHolder(itemView) {
    val imgView: ImageView = itemView.findViewById(R.id.img_item_image)
}

/**
 * Represents a post of type VIDEO in the UI
 */
class VideoViewHolder(itemView: View) : PostViewHolder(itemView) {
    val vidView: StyledPlayerView = itemView.findViewById(R.id.vid_item_video)
}

/**
 * Represents the main profile item in the UI
 */
class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val viewName: TextView = itemView.findViewById(R.id.profile_view_name)
    val viewBio: TextView = itemView.findViewById(R.id.profile_view_bio)
    val viewLocation: TextView = itemView.findViewById(R.id.profile_view_location)
    val viewAvatar: ImageView = itemView.findViewById(R.id.post_view_avatar)
    val btnEdit: Button = itemView.findViewById(R.id.profile_btn_edit)
    val btnSetting: ImageButton = itemView.findViewById(R.id.profile_btn_settings)


}

/**
 * The default Request Listener for every ImagePost
 */
class DefaultRequestListener(private val context: Context, private val imgView: ImageView, private val postId: String) :
    RequestListener<Drawable> {

    /**
     * Log Exception and display error when load fails
     */
    override fun onLoadFailed(
        e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean
    ): Boolean {
        Log.e("Adapter: ", "onLoadFailed, ${e.toString()}")
        return false
    }

    /**
     * Add onclickListener for fullscreen only when recourse is loaded
     */
    override fun onResourceReady(
        resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean
    ): Boolean {
        imgView.setOnClickListener {
            context as FragmentActivity
            val intent = Intent(context, FullscreenActivity::class.java)
            intent.putExtra("postId", postId)
            context.startActivity(intent)
        }
        return false
    }
}