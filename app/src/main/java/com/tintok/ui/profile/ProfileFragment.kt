package com.tintok.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.util.ViewPreloadSizeProvider
import com.tintok.R
import com.tintok.api.UsersService
import com.tintok.api.model.users.ProfilePostPage
import com.tintok.getUserId
import com.tintok.ui.FragmentUsingSnackBar
import com.tintok.util.GlideApp
import retrofit2.Response

private const val ITEM_THRESHOLD: Int = 5
private const val PAGE_SIZE: Int = 20
const val PROFILE_EDIT: Int = 15

/**
 * ProfileFragment shows the own user profile to the user including his own posts.
 * It uses ProfileViewAdapter to render the posts and ProfileEditActivity to allow users to edit their profile.
 */
class ProfileFragment : FragmentUsingSnackBar() {
    // View management
    private lateinit var profilePostsView: RecyclerView
    private lateinit var profilePostsAdapter: ProfileViewAdapter
    private lateinit var layoutManager: LinearLayoutManager

    // Api
    private lateinit var service: UsersService
    private var nextPageToLoad: Int = 0
    private var loading: Boolean = false
    private var lastPageReached: Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        bundle: Bundle?
    ): View {
        service = UsersService(requireContext())
        val root = inflater.inflate(R.layout.fragment_profile, container, false)

        setupRecyclerview(root)
        updateView()
        return root
    }

    /**
     * Set up recyclerview and its components
     */
    private fun setupRecyclerview(root: View) {
        profilePostsView = root.findViewById(R.id.profile_recyclerview)
        profilePostsAdapter = ProfileViewAdapter(this, requireContext().getUserId(), Profile())
        layoutManager = LinearLayoutManager(requireContext())
        profilePostsView.adapter = profilePostsAdapter
        profilePostsView.layoutManager = layoutManager
        profilePostsView.addItemDecoration(
            DividerItemDecoration(
                profilePostsView.context,
                layoutManager.orientation
            )
        )
        loadPosts()
        val preloader: RecyclerViewPreloader<GlideUrl> =
            RecyclerViewPreloader(
                GlideApp.with(this),
                profilePostsAdapter,
                ViewPreloadSizeProvider(),
                10
            )
        profilePostsView.addOnScrollListener(preloader)
        setupScrollListener()
    }

    /**
     * Set up an OnScrollListener to load new data
     * when the difference between the total number of items
     * and the number of items seen has exceeded ITEM_THRESHOLD
     * */
    private fun setupScrollListener() {
        profilePostsView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 && !loading) {
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisItemPos = layoutManager.findFirstVisibleItemPosition()
                    if (ITEM_THRESHOLD > totalItemCount - (firstVisItemPos + visibleItemCount)) {
                        loadPosts()
                    }
                }
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == PROFILE_EDIT && resultCode == Activity.RESULT_OK) {
            updateView()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroyView() {
        profilePostsAdapter.releasePlayers()
        super.onDestroyView()
    }

    override fun onPause() {
        profilePostsAdapter.pauseVideos()
        super.onPause()
    }

    /**
     * Update the view by loading and applying the user's data and avatar
     */
    private fun updateView() {
        LoadUserMeService(requireContext()).loadUser(
            successCallback = {
                profilePostsAdapter.profile.name = getString(
                    R.string.full_name,
                    it.givenName.trim(),
                    it.familyName.trim()
                )
                profilePostsAdapter.profile.bio = it.biography
                profilePostsAdapter.profile.location = it.location
                profilePostsView.post {
                    profilePostsAdapter.notifyDataSetChanged()
                }
            },
            snackBarFragment = this,
            snackBarAction = { updateView() },
        )

    }


    /**
     * Retrieve a new page of content from backend
     */
    private fun loadPosts() {
        if (!loading) {
            loading = true
            if (!lastPageReached) {
                val profilePostsCall =
                    service.api.getOwnPosts(
                        nextPageToLoad,
                        PAGE_SIZE
                    )

                profilePostsCall.enqueue(
                    service.handleApiResponse<ProfilePostPage>(
                        successCallback = {
                            if (isResumed) handleSuccessfulPostsCall(it)
                            loading = false
                        },
                        errorCallback = {
                            loading = false
                            Log.d("Profile: HTTP_API_RESPONSE", it.toString())
                        },
                        failureCallback = {
                            loading = false
                            Log.e("Profile: HTTP_API_EXCEPTION", it.toString())
                        },
                        snackBarFragment = this,
                        snackBarRetryAction = { loadPosts() },
                    )
                )
            } else {
                loading = false
            }
        }
    }


    /**
     * Handle a successful RecommendedPostsPage request with a response code is in [200..300)
     */
    private fun handleSuccessfulPostsCall(response: Response<ProfilePostPage>) {
        when (response.code()) {
            200 -> {
                val postsPage: ProfilePostPage = response.body()!!
                nextPageToLoad++
                lastPageReached = postsPage.last
                profilePostsView.post {
                    profilePostsAdapter.postList.addAll(postsPage.post)
                    profilePostsAdapter.notifyItemRangeInserted(
                        profilePostsAdapter.itemCount - postsPage.post.size,
                        postsPage.post.size
                    )
                }
            }
            204 -> {
                lastPageReached = true
            }
        }
    }

    fun scrollToTop() {
        profilePostsView.scrollToPosition(0)
    }

}