package com.tintok.ui.profile

import android.content.Context
import com.tintok.api.UsersService
import com.tintok.api.model.users.GetUserMe
import com.tintok.ui.FragmentUsingSnackBar

/**
 * LoadUserMeService is used by the ProfileFragment to load the user data.
 */
class LoadUserMeService(context: Context) {

    private val service = UsersService(context)

    /**
     * Load user data and avatar from backend
     */
    fun loadUser(
        successCallback: (user: GetUserMe) -> Unit,
        snackBarFragment: FragmentUsingSnackBar? = null,
        snackBarAction: () -> Unit = {},
    ) {
        service.api.getMe().enqueue(
            service.handleApiResponse<GetUserMe>(
                mustContainSuccessCode = 200,
                successCallback = {
                    successCallback(it.body()!!)
                },
                snackBarFragment = snackBarFragment,
                snackBarRetryAction = snackBarAction,
            )
        )
    }

}