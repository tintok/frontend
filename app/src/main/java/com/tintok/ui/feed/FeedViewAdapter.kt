package com.tintok.ui.feed

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.robertlevonyan.views.chip.Chip
import com.tintok.R
import com.tintok.api.PostsService
import com.tintok.api.model.posts.Post
import com.tintok.api.model.posts.PostStatus
import com.tintok.api.model.posts.PostType
import com.tintok.ui.FullscreenActivity
import com.tintok.util.*
import okhttp3.ResponseBody
import org.ocpsoft.prettytime.PrettyTime
import retrofit2.Call
import java.util.*
import kotlin.collections.ArrayList

/**
 * Adapter for the recyclerview in the [FeedFragment].
 * creates and binds [PostViewHolder] to display in the recyclerview
 * implements user interactions with the ViewHolders
 */
class FeedViewAdapter(
    private val postList: List<Post>,
    private val context: Context,
    private val apiService: PostsService
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ListPreloader.PreloadModelProvider<GlideUrl> {
    private var players: MutableList<Player?> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View

        return when (viewType) {
            R.id.feed_text_item -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.feed_text_post, parent, false)
                TextViewHolder(view)
            }
            R.id.feed_img_item -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.feed_image_post, parent, false)
                ImageViewHolder(view)
            }
            R.id.feed_vid_item -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.feed_video_post, parent, false)
                VideoViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown Post Id: $viewType")
        }
    }

    override fun getItemViewType(position: Int) = when (postList[position].type) {
        PostType.IMAGE -> R.id.feed_img_item
        PostType.VIDEO -> R.id.feed_vid_item
        PostType.TEXT -> R.id.feed_text_item
    }

    override fun getItemCount() = postList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        setupViewHolder(holder as PostViewHolder, postList[position])
        when (holder) {
            is ImageViewHolder -> setupImageViewHolder(holder, postList[position])
            is VideoViewHolder -> setupVideoViewHolder(holder, postList[position])
        }
    }

    /**
     * Assign corresponding values to a basic ViewHolder Layout
     * Called by every ViewHolder
     */
    private fun setupViewHolder(holder: PostViewHolder, post: Post) {
        holder.textView.text = post.text
        holder.username.text = post.author!!.name

        holder.timestamp.text =
            PrettyTime().format(Date(post.created.toEpochSecond() * DateUtils.SECOND_IN_MILLIS))
        loadUserIcon(holder.userIcon, post.author.id)
        post.tags.forEach {
            val view = LayoutInflater.from(holder.tagsView.context)
                .inflate(R.layout.tag_chip_feed, holder.tagsView, false)
            val chip = view.findViewById<Chip>(R.id.tag_chip)
            chip.setText(it)
            holder.tagsView.addView(chip)
        }

        setupInteractionView(holder, post)
        holder.likeButton.setOnClickListener { onLikePost(holder, post) }
        holder.dislikeButton.setOnClickListener { onDislikePost(holder, post) }
    }

    /**
     * Sets the interaction buttons and counter depending on the state of the post.
     */
    private fun setupInteractionView(holder: PostViewHolder, post: Post) {
        // interaction count is difference between likes and dislikes; default to zero if interactions are unknown
        holder.interactionCount.text = ((post.likes ?: 0) - (post.dislikes ?: 0)).toString()
        // show correct button state depending on post status; default to neutral if status is unknown
        when (post.status ?: PostStatus.NEUTRAL) {
            PostStatus.LIKED -> {
                holder.likeButton.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.like_green_24dp, null)
                )
                holder.dislikeButton.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.dislike_gray_24dp, null)
                )
            }
            PostStatus.DISLIKED -> {
                holder.likeButton.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.like_gray_24dp, null)
                )
                holder.dislikeButton.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.dislike_red_24dp, null)
                )
            }
            PostStatus.NEUTRAL -> {
                holder.likeButton.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.like_gray_24dp, null)
                )
                holder.dislikeButton.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.dislike_gray_24dp, null)
                )
            }
        }
    }

    /**
     * Gets called when the like button is pressed. Depending on the state of the post, sends a like
     * or clear request and updates the interaction counts.
     */
    private fun onLikePost(holder: PostViewHolder, post: Post) {
        // disable buttons
        holder.likeButton.isClickable = false
        holder.dislikeButton.isClickable = false
        // send requests and update depending on state; default to neutral if status is unknown
        when (post.status ?: PostStatus.NEUTRAL) {
            // post was liked, set to neutral, decrease likes
            PostStatus.LIKED -> {
                post.status = PostStatus.NEUTRAL
                post.likes = (post.likes ?: 0) - 1
                apiService.api.clearPost(post.id).enqueueAndEnableInteractionButtons(holder)
            }
            // post was neutral, set to liked, increase likes
            PostStatus.NEUTRAL -> {
                post.status = PostStatus.LIKED
                post.likes = (post.likes ?: 0) + 1
                apiService.api.likePost(post.id).enqueueAndEnableInteractionButtons(holder)
            }
            // post was disliked, set to liked, increase likes and decrease dislikes
            PostStatus.DISLIKED -> {
                post.status = PostStatus.LIKED
                post.likes = (post.likes ?: 0) + 1
                post.dislikes = (post.dislikes ?: 0) - 1
                apiService.api.likePost(post.id).enqueueAndEnableInteractionButtons(holder)
            }
        }
        // update interaction buttons and counter
        setupInteractionView(holder, post)
    }

    /**
     * Gets called when the dislike button is pressed. Depending on the state of the post, sends a dislike
     * or clear request and updates the interaction counts.
     */
    private fun onDislikePost(holder: PostViewHolder, post: Post) {
        // disable buttons
        holder.likeButton.isClickable = false
        holder.dislikeButton.isClickable = false
        // send requests and update depending on state; default to neutral if status is unknown
        when (post.status ?: PostStatus.NEUTRAL) {
            // post was liked, set to disliked, decrease likes and increase dislikes
            PostStatus.LIKED -> {
                post.status = PostStatus.DISLIKED
                post.likes = (post.likes ?: 0) - 1
                post.dislikes = (post.dislikes ?: 0) + 1
                apiService.api.dislikePost(post.id).enqueueAndEnableInteractionButtons(holder)
            }
            // post was neutral, set to disliked, increase dislikes
            PostStatus.NEUTRAL -> {
                post.status = PostStatus.DISLIKED
                post.dislikes = (post.dislikes ?: 0) + 1
                apiService.api.dislikePost(post.id).enqueueAndEnableInteractionButtons(holder)
            }
            // post was disliked, set to neutral, decrease dislikes
            PostStatus.DISLIKED -> {
                post.status = PostStatus.NEUTRAL
                post.dislikes = (post.dislikes ?: 0) - 1
                apiService.api.clearPost(post.id).enqueueAndEnableInteractionButtons(holder)
            }
        }
        // update interaction buttons and counter
        setupInteractionView(holder, post)
    }

    /**
     * Enqueues an API call and enables the interaction buttons of the given [PostViewHolder] after the call returns.
     */
    private fun Call<ResponseBody>.enqueueAndEnableInteractionButtons(holder: PostViewHolder) = this.enqueue(
        apiService.handleApiResponse(
            // enable buttons regardless of response
            alwaysDoLast = {
                holder.likeButton.isClickable = true
                holder.dislikeButton.isClickable = true
            }
        )
    )

    /**
     * Loads the profile picture
     */
    private fun loadUserIcon(userIcon: ImageView, id: String) {
        GlideApp.with(context).asDrawable().load(getUserImageUrl(id))
            .applyDefaultProfileOptions()
            .enableGlideCaching()
            .into(userIcon)
    }

    /**
     * Loads the image into the view
     */
    private fun setupImageViewHolder(holder: ImageViewHolder, post: Post) {
        holder.imgView.setOnClickListener(null)
        GlideApp.with(context).asBitmap().load(getPostImageUrl(post.id))
            .applyDefaultImagePostsOptions(context)
            .centerCrop()
            .addListener(DefaultRequestListener(context, holder.imgView, post.id))
            .into(holder.imgView)

        // hide/show text view if post has text
        if (post.text == null)
            holder.textView.visibility = View.GONE
        else holder.textView.visibility = View.VISIBLE
    }

    /**
     * Load the video into the view
     */
    private fun setupVideoViewHolder(holder: VideoViewHolder, post: Post) {
        val player = getDefaultPlayerBuilder(context).build()
        players.add(player)
        holder.vidView.player = player

        player.setMediaItem(MediaItem.fromUri(getVideoUri(post.id)))
        player.prepare()

        // hide/show text view if post has text
        if (post.text == null)
            holder.textView.visibility = View.GONE
        else holder.textView.visibility = View.VISIBLE
    }

    /**
     * release video players when the view gets recycled
     */
    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        if (holder is PostViewHolder) {
            holder.tagsView.removeAllViews()
        }

        if (holder is VideoViewHolder) {
            val player = holder.vidView.player
            players.remove(player)
            player?.release()
        }
        super.onViewRecycled(holder)
    }

    /**
     * pause videos when the view is not visible anymore
     */
    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        if (holder is VideoViewHolder) {
            holder.vidView.player?.pause()
        }
        super.onViewDetachedFromWindow(holder)
    }

    override fun getPreloadItems(position: Int): MutableList<GlideUrl> {
        val list: MutableList<GlideUrl> = ArrayList()
        if (position < postList.size) {
            list.add(getUserImageUrl(postList[position].author!!.id))
            if (postList[position].type == PostType.IMAGE)
                list.add(getPostImageUrl(postList[position].id))
        }
        return list
    }


    override fun getPreloadRequestBuilder(item: GlideUrl): RequestBuilder<*> =
        GlideApp.with(context).asBitmap().load(item)

    /**
     * pause all videos
     */
    fun pauseVideos() {
        players.forEach { it?.pause() }
    }

    /**
     * release all video players
     */
    fun releasePlayers() {
        players.forEach { it?.release() }
        players = ArrayList()
    }
}

/**
 * Represents a post in the UI
 */
sealed class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val textView: TextView = itemView.findViewById(R.id.feed_post_text)
    val username: TextView = itemView.findViewById(R.id.username)
    val userIcon: ImageView = itemView.findViewById(R.id.user_icon)
    val timestamp: TextView = itemView.findViewById(R.id.time)
    val tagsView: LinearLayout = itemView.findViewById(R.id.feed_tags_view)
    val likeButton: ImageButton = itemView.findViewById(R.id.feed_btn_like)
    val interactionCount: TextView = itemView.findViewById(R.id.feed_interaction_count)
    val dislikeButton: ImageButton = itemView.findViewById(R.id.feed_btn_dislike)
}

/**
 * Represents a post of type TEXT in the UI
 */
class TextViewHolder(itemView: View) : PostViewHolder(itemView)

/**
 * Represents a post of type IMAGE in the UI
 */
class ImageViewHolder(itemView: View) : PostViewHolder(itemView) {
    val imgView: ImageView = itemView.findViewById(R.id.img_item_image)
}

/**
 * Represents a post of type VIDEO in the UI
 */
class VideoViewHolder(itemView: View) : PostViewHolder(itemView) {
    val vidView: StyledPlayerView = itemView.findViewById(R.id.vid_item_video)
}

/**
 * The default Request Listener for every ImagePost
 */
class DefaultRequestListener(
    private val context: Context, private val imgView: ImageView, private val postId: String
) : RequestListener<Bitmap> {

    /**
     * Log Exception and display error when load fails
     */
    override fun onLoadFailed(
        e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean
    ): Boolean {
        Log.e("Adapter: ", "onLoadFailed, ${e.toString()}")
        return false
    }

    /**
     * Add onclickListener for fullscreen only when recourse is loaded
     */
    override fun onResourceReady(
        resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean
    ): Boolean {
        imgView.setOnClickListener {
            context as FragmentActivity
            val intent = Intent(context, FullscreenActivity::class.java)
            intent.putExtra("postId", postId)
            context.startActivity(intent)
        }
        return false
    }
}