package com.tintok.ui.feed

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.util.ViewPreloadSizeProvider
import com.tintok.R
import com.tintok.api.PostsService
import com.tintok.api.model.posts.FeedPostPage
import com.tintok.api.model.posts.Post
import com.tintok.ui.FragmentUsingSnackBar
import com.tintok.util.GlideApp
import retrofit2.Response
import java.time.ZonedDateTime

private const val ITEM_THRESHOLD: Int = 5
private const val PAGE_SIZE: Int = 20

/**
 * Home Feed Fragment
 * Displays a list of post created by matched users
 */
class FeedFragment : FragmentUsingSnackBar() {

    // Views
    private lateinit var recyclerView: RecyclerView
    private lateinit var feedViewAdapter: FeedViewAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var loader: ProgressBar
    private lateinit var layoutNoContent: LinearLayout

    // API related
    private val postList: MutableList<Post> = ArrayList()
    private lateinit var apiService: PostsService
    private lateinit var currentTimestamp: ZonedDateTime
    private var nextPageToLoad: Int = 0
    private var lastPageReached: Boolean = false
    private var loading: Boolean = false

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_feed, container, false)
        apiService = PostsService(requireContext())
        currentTimestamp = ZonedDateTime.now()
        loader = root.findViewById(R.id.feed_loader)
        layoutNoContent = root.findViewById(R.id.feed_layout_no_content)
        setupRecyclerview(root)
        setupScrollListener()

        val navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
        root.findViewById<Button>(R.id.feed_button_no_content).setOnClickListener {
            navController.navigate(R.id.navigation_recommended)
        }

        return root
    }

    override fun onPause() {
        feedViewAdapter.pauseVideos()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        if (postList.isEmpty())
            loadPosts()
        else loader.visibility = View.GONE
    }

    override fun onDestroyView() {
        feedViewAdapter.releasePlayers()
        super.onDestroyView()
    }

    /**
     * Set up an OnScrollListener to load new data
     * when the difference between the total number of items
     * and the number of items seen has exceeded ITEM_THRESHOLD
     * */
    private fun setupScrollListener() {
        recyclerView.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 && !loading) {
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisItemPos = layoutManager.findFirstVisibleItemPosition()
                    if (ITEM_THRESHOLD > totalItemCount - (firstVisItemPos + visibleItemCount)) {
                        loadPosts()
                    }
                }
            }
        })
    }

    /**
     * Set up recyclerview and its components
     */
    private fun setupRecyclerview(root: View) {
        recyclerView = root.findViewById(R.id.feed_recyclerview)
        feedViewAdapter = FeedViewAdapter(postList, requireContext(), apiService)
        layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = feedViewAdapter
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                layoutManager.orientation
            )
        )
        val preloader: RecyclerViewPreloader<GlideUrl> =
            RecyclerViewPreloader(
                GlideApp.with(requireContext()),
                feedViewAdapter,
                ViewPreloadSizeProvider(),
                10
            )
        recyclerView.addOnScrollListener(preloader)
    }


    /**
     * Retrieve a new page of content from backend
     */
    fun loadPosts() {
        if (!loading) {
            loading = true
            if (!lastPageReached) {
                val feedPostsCall = apiService.api.getPostFeed(
                    PAGE_SIZE,
                    nextPageToLoad,
                    currentTimestamp
                )

                feedPostsCall.enqueue(
                    apiService.handleApiResponse<FeedPostPage>(
                        successCallback = {
                            handleSuccessfulPostsCall(it)
                        },
                        snackBarFragment = this,
                        snackBarRetryAction = { loadPosts() },
                        alwaysDoLast = { loading = false },
                    )
                )
            }
        }
    }

    /**
     * Handle a successful RecommendedPostsPage request with a response code is in [200..300)
     */
    private fun handleSuccessfulPostsCall(response: Response<FeedPostPage>) {
        if (isResumed) {
            when (response.code()) {
                200 -> {
                    val postsPage: FeedPostPage = response.body()!!
                    postList.addAll(postsPage.post)
                    nextPageToLoad++
                    lastPageReached = postsPage.last
                    recyclerView.post {
                        loader.visibility = View.GONE
                        layoutNoContent.visibility
                        feedViewAdapter.notifyItemRangeInserted(
                            postList.size - postsPage.post.size,
                            postsPage.post.size
                        )
                    }
                }
                204 -> {
                    lastPageReached = true
                    loader.visibility = View.GONE
                    if (postList.isEmpty())
                        layoutNoContent.visibility = View.VISIBLE
                }
            }
        }
    }

    fun scrollToTop() {
        recyclerView.scrollToPosition(0)
    }
}

