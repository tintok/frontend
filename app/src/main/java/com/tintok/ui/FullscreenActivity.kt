package com.tintok.ui

import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.tintok.R
import com.tintok.util.GlideApp
import com.tintok.util.getPostImageUrl

/**
 * Displays the Image of an ImagePost in fullscreen
 */
class FullscreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen_image)

        val img: ImageView = findViewById(R.id.fullscreen_image)

        GlideApp.with(this).asBitmap().load(getPostImageUrl(intent.getStringExtra("postId")!!))
            .applyDefaultImagePostsOptions(this)
            .fitCenter()
            .into(img)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }
}
