package com.tintok.ui.post

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.navigation.Navigation.findNavController
import com.abdeveloper.library.MultiSelectDialog
import com.abdeveloper.library.MultiSelectDialog.SubmitCallbackListener
import com.abdeveloper.library.MultiSelectModel
import com.deepakkumardk.videopickerlib.EasyVideoPicker
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.mobsandgeeks.saripaar.annotation.Length
import com.mobsandgeeks.saripaar.annotation.Order
import com.robertlevonyan.views.chip.Chip
import com.robertlevonyan.views.chip.OnCloseClickListener
import com.tintok.R
import com.tintok.api.PostsService
import com.tintok.api.TagsService
import com.tintok.api.model.Location
import com.tintok.api.model.posts.MediaAttributes
import com.tintok.api.model.posts.TextPost
import com.tintok.ui.*
import com.tintok.util.*
import okhttp3.ResponseBody
import java.io.File
import kotlin.collections.component1
import kotlin.collections.component2

/**
 * PostFragment shows the form to create new posts. 
 * The User can create new posts with text, and add a picture or video to it and publish it afterwards.
 */
class PostFragment : FragmentUsingPermissions() {

    // Services
    private lateinit var postsService: PostsService
    private lateinit var tagsService: TagsService
    private lateinit var player: SimpleExoPlayer
    private lateinit var inflaterInstance: LayoutInflater
    private lateinit var validator: InputValidator

    // Views
    @Length(max = 512, messageResId = R.string.input_error_max_length_512)
    @Order(0)
    private lateinit var textInput: EditText
    private lateinit var tagsView: LinearLayout
    private lateinit var viewAvatar: ImageView
    private lateinit var imagePreview: ImageView
    private lateinit var playerView: StyledPlayerView
    private lateinit var btnAddTags: Button
    private lateinit var btnRemoveMedia: Button
    private lateinit var btnAddImage: ImageButton
    private lateinit var btnAddVideo: ImageButton
    private lateinit var btnPublishPost: ImageButton
    private lateinit var loader: ProgressBar
    private lateinit var loaderMessage: TextView
    private lateinit var scrollView: ScrollView

    // Post data
    private var postImageFile: File? = null
    private var postVideoFile: File? = null
    private val tagsDialog = MultiSelectDialog()
    private var availableTags = emptyMap<Int, String>()
    private var selectedTags = mutableMapOf<Int, String>()
    private var loading = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        bundle: Bundle?
    ): View {
        inflaterInstance = inflater
        postsService = PostsService(requireContext())
        tagsService = TagsService(requireContext())

        val root = inflater.inflate(R.layout.fragment_post, container, false)
        scrollView = root.findViewById(R.id.post_scroll_view)
        textInput = root.findViewById(R.id.post_input_text)
        tagsView = root.findViewById(R.id.post_tags_view)
        viewAvatar = root.findViewById(R.id.post_view_avatar)
        imagePreview = root.findViewById(R.id.post_image_view)
        playerView = root.findViewById(R.id.post_player_view)
        btnAddTags = root.findViewById(R.id.post_btn_add_tags)
        btnRemoveMedia = root.findViewById(R.id.post_btn_remove_media)
        btnAddImage = root.findViewById(R.id.post_btn_add_image)
        btnAddVideo = root.findViewById(R.id.post_btn_add_video)
        btnPublishPost = root.findViewById(R.id.post_btn_publish_post)
        loader = root.findViewById(R.id.post_loader)
        loaderMessage = root.findViewById(R.id.post_loader_message)

        requireContext().loadUserAvatar(viewAvatar)
        prepareTagsDialog()
        loadTagList()
        initializeVideoPlayer()
        setupValidationAndButtons()

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (this::player.isInitialized)
            player.release()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            try {
                when (requestCode) {
                    REQUEST_CODE_VIDEO_PICKER -> {
                        val videos = EasyVideoPicker.getSelectedVideos(data)
                        addMediaToPost(File(videos!!.first().videoPath!!), true)
                    }
                    ImagePicker.REQUEST_CODE -> {
                        addMediaToPost(ImagePicker.getFile(data)!!, false)
                    }

                }
            } catch (error: Exception) {
                Log.e(
                    "PostFragmentActivityResult",
                    "Could not process activity result: ${error.message}"
                )
                showToast(requireContext(), R.string.error_media_selection)
            }
        }
    }

    /**
     * Prepare post by retrieving selected tags and getting users location
     */
    private fun preparePost() {
        snackBar?.dismiss()
        if (selectedTags.isEmpty()) {
            showToast(requireContext(), R.string.error_empty_tags)
        } else {
            updateLoader(true)
            locationActionWithPermissionCheck(
                LocationCallbacks(
                    first = { updateLoader(true) },
                    success = {
                        determinePostType(
                            it,
                            selectedTags.map { (_, tagLabel) -> tagLabel },
                            textInput.lessWhiteSpacedText()
                        )
                    },
                    error = { updateLoader(false) },
                    retry = { preparePost() },
                )
            )
        }
    }

    /**
     * Setup input validation and button actions
     */
    private fun setupValidationAndButtons() {
        validator = InputValidator(this, requireContext())
            .registerInputs(validateTillInputs = listOf(textInput))
            .setValidationCallbacks(
                { btnPublishPost.isEnabled = true },
                { btnPublishPost.isEnabled = false },
            )

        btnAddTags.setOnClickListener {
            tagsDialog.preSelectIDsList(ArrayList(selectedTags.keys))
            tagsDialog.show(parentFragmentManager, "multiSelectDialog")
        }
        btnRemoveMedia.setOnClickListener { removeMediaFromPost() }
        btnAddImage.setOnClickListener {
            mediaActionWithPermissionCheck(MediaCallback {
                getImagePicker(this).start()
            })
        }
        btnAddVideo.setOnClickListener {
            mediaActionWithPermissionCheck(MediaCallback {
                startVideoPicker(this)
            })
        }
        btnPublishPost.setOnClickListener {
            hideKeyboard(requireContext(), it)
            preparePost()
        }

        locationDeniedCallback = {
            Log.d(javaClass.name, "Location denied callback!")
            updateLoader(false)
        }
        snackBarAnchorViewId = R.id.post_bottom_buttons
    }

    /**
     * Update the visibility of the bottom action view to show a spinner when loading
     * and enable/disable the whole scroll view
     */
    private fun updateLoader(loading: Boolean) {
        if (isVisible && this.loading != loading) {
            this.loading = loading
            btnAddImage.visibility = if (loading) View.GONE else View.VISIBLE
            btnAddVideo.visibility = if (loading) View.GONE else View.VISIBLE
            btnPublishPost.visibility = if (loading) View.GONE else View.VISIBLE
            loader.visibility = if (loading) View.VISIBLE else View.GONE
            loaderMessage.visibility = if (loading) View.VISIBLE else View.GONE
            loaderMessage.text = getString(R.string.publishing_post)
            playerView.useController = !loading
            scrollView.deepForEach {
                if (this is Chip) {
                    this.closable = !loading
                    this.chipBackgroundColor =
                        if (loading) ContextCompat.getColor(requireContext(), R.color.gray)
                        else ContextCompat.getColor(requireContext(), R.color.chip_background)
                } else {
                    isEnabled = !loading
                }
            }
        }
    }

    /**
     * Add new player API to appropriate view
     */
    private fun initializeVideoPlayer() {
        player = SimpleExoPlayer.Builder(requireContext()).build()
        playerView.player = player
    }

    /**
     * Add a video to the preview and save file to be uploaded
     */
    private fun addMediaToPost(file: File, mediaIsVideo: Boolean) {
        if (mediaIsVideo && file.sizeInMb >= VIDEO_SELECTION_MAX_MB) {
            showToast(requireContext(), R.string.error_video_too_large)
            return
        }
        val supportedExtensions =
            if (mediaIsVideo) supportedVideoExtensions else supportedImageExtensions
        if (file.extension in supportedExtensions) {
            Log.d("FileUri", file.toUri().toString())
            postVideoFile = if (mediaIsVideo) file else null
            postImageFile = if (mediaIsVideo) null else file
            if (mediaIsVideo) {
                player.setMediaItem(MediaItem.fromUri(file.toUri()))
                player.prepare()
            } else {
                imagePreview.setImageURI(file.toUri())
                player.pause()
            }
            imagePreview.visibility = if (mediaIsVideo) View.GONE else View.VISIBLE
            playerView.visibility = if (mediaIsVideo) View.VISIBLE else View.GONE
            btnRemoveMedia.visibility = View.VISIBLE
        } else {
            showToast(requireContext(), R.string.error_media_format)
        }
    }

    /**
     * Remove any media which is attached to the post
     */
    private fun removeMediaFromPost() {
        postImageFile = null
        postVideoFile = null
        imagePreview.visibility = View.GONE
        playerView.visibility = View.GONE
        btnRemoveMedia.visibility = View.GONE
        player.pause()
    }

    /**
     * Set dialog options
     */
    private fun prepareTagsDialog() {
        tagsDialog
            .titleSize(0f)
            .positiveText(getString(R.string.done))
            .negativeText(getString(R.string.cancel))
            .setMinSelectionLimit(0)
            .setMaxSelectionLimit(8)
            .onSubmit(handleTagDialogSubmission)
    }

    /**
     * Load available tags from backend
     */
    private fun loadTagList() {
        tagsService.api.getAvailableTags().enqueue(
            tagsService.handleApiResponse<List<String>>(
                mustContainSuccessCode = 200,
                successCallback = {
                    availableTags = it.body()!!.mapIndexed { index, tagLabel ->
                        index to tagLabel
                    }.toMap()
                    val selectOptions =
                        availableTags.map { (index, tag) -> MultiSelectModel(index, tag) }
                    tagsDialog.multiSelectList(ArrayList(selectOptions))
                }, snackBarFragment = this,
                snackBarRetryAction = { loadTagList() }
            )
        )
    }

    /**
     * Handle the submission of the tags dialog
     */
    private val handleTagDialogSubmission = object : SubmitCallbackListener {
        override fun onSelected(
            selectedIds: ArrayList<Int>,
            selectedNames: ArrayList<String>,
            dataString: String
        ) {
            selectedTags = selectedIds.map {
                it to (availableTags[it] ?: error("Could not find tagLabel of selected tag."))
            }.toMap().toMutableMap()
            tagsView.removeAllViews()
            selectedTags.forEach { (index, tagLabel) ->
                val root = inflaterInstance.inflate(R.layout.tag_chip_post, tagsView, false)
                val chip = root.findViewById<Chip>(R.id.tag_chip)
                chip.setText(tagLabel)
                chip.onCloseClickListener = OnCloseClickListener {
                    tagsView.removeView(chip)
                    selectedTags.remove(index)
                }
                tagsView.addView(chip)
            }
            if (selectedTags.isEmpty()) {
                showToast(requireContext(), R.string.error_empty_tags)
            }
        }

        override fun onCancel() {}
    }

    /**
     * Determine if post is of type image/video/text and send appropriate request
     */
    private fun determinePostType(locationAttribute: Location, tags: List<String>, text: String?) {
        updateLoader(true) // Update loader again in case location permission was just granted

        val mediaAttributes = MediaAttributes(locationAttribute, tags, text)
        when {
            postImageFile != null -> sendImagePost(mediaAttributes)
            postVideoFile != null -> sendVideoPost(mediaAttributes)
            text != null -> sendTextPost(TextPost(locationAttribute, tags, text))
            else -> {
                updateLoader(false)
                showToast(requireContext(), R.string.error_empty_text_and_media)
            }
        }
    }

    /**
     * Send a new image post to backend
     */
    private fun sendImagePost(mediaAttributes: MediaAttributes) {
        val fileBody = getMediaUploadBody(postImageFile!!, "image")
        postsService.api.postImagePost(mediaAttributes, fileBody).enqueue(
            handleApiResponseOfPostCreation { updateLoader(false) }
        )
    }

    /**
     * Compress and send a video post to backend
     */
    private fun sendVideoPost(mediaAttributes: MediaAttributes) {
        loaderMessage.text = getString(R.string.compressing_video)
        compressVideo(
            requireContext(),
            postVideoFile!!,
            successCallback = { compressedVideo ->
                val fileBody = getMediaUploadBody(compressedVideo, "video", "video")
                val postCall = postsService.api.postVideoPost(mediaAttributes, fileBody)
                loaderMessage.text = getString(R.string.publishing_post)

                postCall.enqueue(
                    handleApiResponseOfPostCreation {
                        updateLoader(false)
                        compressedVideo.delete()
                    }
                )
            },
            errorCallback = { updateLoader(false) }
        )
    }

    /**
     * Send a new text post to backend
     */
    private fun sendTextPost(textPost: TextPost) =
        postsService.api.postTextPost(textPost).enqueue(
            handleApiResponseOfPostCreation { updateLoader(false) }
        )

    /**
     * Default success / error / failure handling of an API response
     */
    private fun handleApiResponseOfPostCreation(alwaysDoLast: () -> Unit = {}) =
        postsService.handleApiResponse<ResponseBody>(
            mustContainSuccessCode = 201,
            successCallback = {
                showToast(requireContext(), R.string.post_published)
                switchToFeed()
            },
            snackBarFragment = this,
            snackBarRetryAction = { preparePost() },
            alwaysDoLast = alwaysDoLast,
        )

    /**
     * Switch to the matches feed tab after successfully publishing a post
     */
    private fun switchToFeed() {
        if (isResumed) {
            val navController = findNavController(requireActivity(), R.id.nav_host_fragment)
            navController.navigate(R.id.navigation_home)
        }
    }
}