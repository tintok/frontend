package com.tintok.ui.chat

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.util.ViewPreloadSizeProvider
import com.tintok.R
import com.tintok.api.ChatsService
import com.tintok.api.model.chats.ChatConversation
import com.tintok.api.model.chats.ChatList
import com.tintok.api.model.fcm.FirebaseMessage
import com.tintok.ui.FragmentUsingSnackBar
import com.tintok.util.*
import retrofit2.Response

private const val ITEM_THRESHOLD: Int = 5
private const val PAGE_SIZE = 15

/**
 * ChatList represents the fragment that shows chats with other people.
 * The user can click on the chatlistItem rendered by the RecyclerView and switch to the ChatActivity to chat with the person.
 */
class ChatListFragment : FragmentUsingSnackBar() {
    private var conversations: MutableList<ChatConversation> = ArrayList()
    private lateinit var service: ChatsService
    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: ChatListRecyclerView
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var progressBar: ProgressBar
    private lateinit var noContentDescription: TextView
    private lateinit var messageListener: BroadcastReceiver
    private lateinit var notificationManager: NotificationManager
    private var nextPageToLoad: Int = 0
    private var lastPageReached: Boolean = false
    private var loading: Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_chat_list, container, false)
        progressBar = root.findViewById(R.id.chat_overview_loader)
        noContentDescription = root.findViewById(R.id.chat_no_conversations)
        notificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        service = ChatsService(requireContext())
        setupRecyclerView(root)
        setupScrollListener()
        setupGlidePreloader()
        setupNotificationListener()

        return root
    }

    /**
     * Loads messages on Spawn of ChatOverview
     */
    private fun loadConversations() {
        if (!loading && !lastPageReached) {
            setLoading(true)
            service.api.listChats(PAGE_SIZE, nextPageToLoad).enqueue(
                service.handleApiResponse<ChatList>(
                    successCallback = {
                        handleSuccessCallBack(it)
                    },
                    alwaysDoLast = { setLoading(false) },
                    snackBarFragment = this,
                    snackBarRetryAction = { loadConversations() },
                )
            )
        }
    }

    /**
     * Set state of ChatOverview Loaders
     */
    private fun setLoading(state: Boolean) {
        loading = state
        if (conversations.size <= 0 || !state) progressBar.visibility =
            if (state) View.VISIBLE else View.GONE
    }

    /**
     * Handle a successful Response from the API to setup scroll loading
     */
    private fun handleSuccessCallBack(it: Response<ChatList>) {
        if (isResumed) {
            when (it.code()) {
                200 -> {
                    val resp = it.body()!!
                    val loadedConversations = resp.messages
                    recyclerView.post {
                        val wasEmpty = this.conversations.isEmpty()
                        this.conversations.addAll(loadedConversations)
                        if (wasEmpty) {
                            recyclerViewAdapter.notifyDataSetChanged()
                        } else {
                            recyclerViewAdapter.notifyItemRangeInserted(
                                this.conversations.size - loadedConversations.size,
                                this.conversations.size
                            )
                        }
                    }

                    nextPageToLoad++
                    lastPageReached = resp.last
                }
                204 -> {
                    lastPageReached = true
                    noContentDescription.visibility = View.VISIBLE
                }
            }
        }
    }

    /**
     * Setup the recyclerview for the ChatOverview
     */
    private fun setupRecyclerView(root: View) {
        recyclerView = root.findViewById(R.id.chat_recyclerview)
        recyclerViewAdapter =
            ChatListRecyclerView(conversations, requireContext(), service, noContentDescription)
        recyclerView.adapter = recyclerViewAdapter
        layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                layoutManager.orientation
            )
        )
    }

    /**
     * Setup the scroll listener
     */
    private fun setupScrollListener() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 && !loading) {
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisItemPos = layoutManager.findFirstVisibleItemPosition()
                    if (ITEM_THRESHOLD > totalItemCount - (firstVisItemPos + visibleItemCount)) {
                        loadConversations()
                    }
                }
            }
        })
    }

    /**
     * Setup preloader for images
     */
    private fun setupGlidePreloader() {
        val preloader: RecyclerViewPreloader<GlideUrl> =
            RecyclerViewPreloader(
                GlideApp.with(requireContext()),
                recyclerViewAdapter,
                ViewPreloadSizeProvider(),
                15
            )
        recyclerView.addOnScrollListener(preloader)
    }

    override fun onResume() {
        super.onResume()
        reloadConversations()
    }

    /**
     * Clear the conversations from the current recyclerView and reload messages
     */
    private fun reloadConversations() {
        this.conversations.clear()
        lastPageReached = false
        nextPageToLoad = 0
        recyclerView.post {
            recyclerViewAdapter.notifyDataSetChanged()
        }
        loadConversations()
    }

    /**
     * Setup Notification Listener that reloads the chat overview when a new message is received
     */
    private fun setupNotificationListener() {
        messageListener = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (isResumed) {
                    reloadConversations()
                    val message =
                        intent?.getSerializableExtra(MESSAGE_INTENT_EXTRA_NAME) as FirebaseMessage?
                    message?.let {
                        notificationManager.cancelGroupNotification(
                            NOTIFICATION_GROUP_CHATS,
                            it.baseNotificationId,
                            it.summaryNotificationId,
                        )
                    }
                }
            }
        }
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(
            messageListener, IntentFilter(
                MESSAGE_LISTENER_NAME
            )
        )
    }

    override fun onDestroyView() {
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(messageListener)
        super.onDestroyView()
    }
}