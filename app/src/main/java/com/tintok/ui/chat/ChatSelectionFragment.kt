package com.tintok.ui.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.tintok.R
import com.tintok.ui.matchlist.MatchListFragment

/**
 * ChatSelection shows the top selection bar shown on the chat and matches tab.
 * By clicking on one of the elements provided in the ChatSelection, the user can switch between tabs.
 */
class ChatSelectionFragment : Fragment() {
    private lateinit var tabLayout: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_chat_list_tabs, container, false)
        childFragmentManager.beginTransaction()
            .replace(R.id.tab_selection_container, ChatListFragment())
            .commit()
        tabLayout = root.findViewById(R.id.chat_tabs)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        childFragmentManager.beginTransaction()
                            .replace(R.id.tab_selection_container, ChatListFragment()).commit()
                    }
                    1 -> {
                        childFragmentManager.beginTransaction()
                            .replace(R.id.tab_selection_container, MatchListFragment()).commit()
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
        return root
    }

}