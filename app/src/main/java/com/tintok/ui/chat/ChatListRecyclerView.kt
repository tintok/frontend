package com.tintok.ui.chat

import android.content.Context
import android.content.Intent
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.model.GlideUrl
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.tintok.R
import com.tintok.api.ChatsService
import com.tintok.api.model.chats.ChatConversation
import com.tintok.util.GlideApp
import com.tintok.util.getUserImageUrl
import com.tintok.util.onlySingleSpaces
import org.ocpsoft.prettytime.PrettyTime
import java.util.*

const val CHAT_ID = "com.tintok.chat.id"
const val CHAT_NAME = "com.tintok.chat.name"

/**
 * Adapter for the recyclerview in the [ChatListFragment].
 * creates and binds [ChatConversationHolder] to display conversations in the recyclerview
 * implements user interactions with the ViewHolders
 */
class ChatListRecyclerView(
    private var conversations: MutableList<ChatConversation>,
    private val context: Context,
    private val api: ChatsService,
    private val noContentDesc: TextView
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), ListPreloader.PreloadModelProvider<GlideUrl> {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.chat_list_entry, parent, false)
        return ChatConversationHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ChatConversationHolder -> {
                val conversation = conversations[position]
                holder.messageText.text = conversation.message.onlySingleSpaces()
                holder.username.text = conversation.chat_partner.name
                holder.timestamp.text =
                    PrettyTime().format(
                        Date(
                            conversation.last_message.toEpochSecond() * DateUtils.SECOND_IN_MILLIS
                        )
                    )
                loadUserIcon(holder.icon, conversation.chat_partner.id)
                if (conversations[position].unread > 0) {
                    holder.unread.text = conversations[position].unread.toString()
                    holder.unread.visibility = View.VISIBLE
                } else
                    holder.unread.visibility = View.INVISIBLE

                // Switch to the chat if the user clicks on it
                holder.itemView.setOnClickListener {
                    switchToChat(it.context, conversation, position)
                }

                holder.itemView.setOnLongClickListener {
                    MaterialAlertDialogBuilder(context)
                        .setTitle(R.string.chat_removal_title)
                        .setMessage(
                            String.format(
                                context.getString(R.string.chat_removal_msg),
                                conversations[holder.adapterPosition].chat_partner.name
                            )
                        )
                        .setNegativeButton(R.string.cancel) { _, _ -> }
                        .setPositiveButton(R.string.delete) { _, _ ->
                            api.api.leaveChat(conversations[holder.adapterPosition].chat_partner.id)
                                .enqueue(api.handleApiResponse(
                                    mustContainSuccessCode = 204,
                                    successCallback = {
                                        conversations.removeAt(holder.adapterPosition)
                                        notifyItemRemoved(holder.adapterPosition)
                                        if (conversations.size <= 0) {
                                            noContentDesc.visibility = View.VISIBLE
                                        }
                                    }
                                ))
                        }.show()

                    return@setOnLongClickListener true
                }
            }
            else -> throw java.lang.IllegalArgumentException("Invalid Type")
        }
    }

    /**
     * Loads the profile picture
     */
    private fun loadUserIcon(userIcon: ImageView, id: String) {
        GlideApp.with(context).asDrawable().load(getUserImageUrl(id))
            .applyDefaultProfileOptions()
            .enableGlideCaching()
            .into(userIcon)
    }

    /**
     * Switches to a chat when
     */
    private fun switchToChat(ctx: Context, conversation: ChatConversation, position: Int) {
        // Manually mark chat as read so you do not need to reload chats if user switches back to view
        this.conversations[position].unread = 0
        notifyItemChanged(position)
        val intent = Intent(ctx, ChatActivity::class.java)
        intent.putExtra(CHAT_ID, conversation.chat_partner.id)
        intent.putExtra(CHAT_NAME, conversation.chat_partner.name)
        startActivity(ctx, intent, null)
    }

    override fun getItemCount() = conversations.size

    /**
     * ChatConversationHolder represents a Conversation shown to a user
     */
    class ChatConversationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById(R.id.chat_item_text)
        var username: TextView = itemView.findViewById(R.id.chat_name)
        var timestamp: TextView = itemView.findViewById(R.id.chat_time)
        var icon: ImageView = itemView.findViewById(R.id.chat_overview_icon)
        var unread: TextView = itemView.findViewById(R.id.chat_unread)


    }

    override fun getPreloadItems(position: Int): MutableList<GlideUrl> {
        val list: MutableList<GlideUrl> = ArrayList()
        if (position < conversations.size)
            list.add(getUserImageUrl(conversations[position].chat_partner.id))
        return list
    }

    override fun getPreloadRequestBuilder(item: GlideUrl): RequestBuilder<*> =
        GlideApp.with(context).asDrawable().load(item)
}