package com.tintok.ui.chat

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.mobsandgeeks.saripaar.annotation.Length
import com.mobsandgeeks.saripaar.annotation.Order
import com.tintok.R
import com.tintok.api.ChatsService
import com.tintok.api.model.chats.ChatHistory
import com.tintok.api.model.chats.ChatMessage
import com.tintok.api.model.chats.ChatMessageSend
import com.tintok.api.model.fcm.FirebaseMessage
import com.tintok.api.model.fcm.MessageType
import com.tintok.api.model.fcm.baseNotificationId
import com.tintok.util.*
import java.time.ZonedDateTime

private const val ITEM_THRESHOLD: Int = 5
private const val PAGE_SIZE = 20

/**
 * ChatActivity represents the view that an user uses to chat with other people.
 * It renders the messages between two users using an RecyclerView and is responsible for loading and updating messages as well as sending messages.
 */
class ChatActivity : AppCompatActivity() {
    @Length(max = 512, trim = true, messageResId = R.string.input_error_max_length_512)
    @Order(0)
    private lateinit var textInputField: TextInputEditText
    private lateinit var textSendButton: ImageButton
    private lateinit var service: ChatsService
    private lateinit var recyclerView: RecyclerView
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var recyclerViewAdapter: ChatMessageRecyclerAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var chatID: String
    private lateinit var validator: InputValidator
    private lateinit var chatname: String
    private lateinit var messageListener: BroadcastReceiver
    private lateinit var noContentScreen: TextView
    private lateinit var notificationManager: NotificationManager
    private val messages: MutableList<ChatMessage> = ArrayList()
    private var nextPageToLoad: Int = 0
    private var lastPageReached: Boolean = false
    private var loading: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        // Initialize Variables
        service = ChatsService(this)
        textInputField = findViewById(R.id.chat_input_textfield)
        textSendButton = findViewById(R.id.chat_sendbutton)
        progressBar = findViewById(R.id.chat_activity_progressbar)
        noContentScreen = findViewById(R.id.chat_activity_no_conversation)
        chatID = intent.getStringExtra(CHAT_ID)!!
        chatname = intent.getStringExtra(CHAT_NAME)!!
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        setChatTitle(chatname)
        loadChats()
        setupChatButtons()
        setupValidator()
        setupRecyclerView()
        setupScrollListener()
        setupMessageListener()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        switchToChatOverview()
        return true
    }

    override fun onBackPressed() {
        switchToChatOverview()
    }

    /**
     * Setup message listener for new chat messages when Chats are open.
     * Clear all notifications with this chat partner
     */
    private fun setupMessageListener() {
        messageListener = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Log.d("MessageListener", "New chat message received.")
                if (intent?.extras != null) {
                    val message =
                        intent.getSerializableExtra(MESSAGE_INTENT_EXTRA_NAME) as FirebaseMessage?

                    if (message?.message != null && message.id == chatID) {
                        addFirebaseMessageToChat(message)
                    }
                }
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(
            messageListener, IntentFilter(
                MESSAGE_LISTENER_NAME
            )
        )
        notificationManager.cancelGroupNotification(
            NOTIFICATION_GROUP_CHATS,
            baseNotificationId(chatID, MessageType.CHAT.ordinal),
            MessageType.CHAT.ordinal,
        )
        notificationManager.cancelGroupNotification(
            NOTIFICATION_GROUP_MATCHES,
            baseNotificationId(chatID, MessageType.MATCH.ordinal),
            MessageType.MATCH.ordinal,
        )
    }

    /**
     * Add the new message to the recycler view and cancel the notification
     */
    private fun addFirebaseMessageToChat(message: FirebaseMessage) {
        recyclerView.post {
            this.messages.add(
                0,
                ChatMessage(
                    ZonedDateTime.now().toString(),
                    ZonedDateTime.now(),
                    message.message!!,
                    false
                )
            )
            noContentScreen.visibility = View.GONE
            if (this.messages.size < 2) {
                recyclerViewAdapter.notifyDataSetChanged()
            } else {
                recyclerViewAdapter.notifyItemInserted(0)
            }
            recyclerView.smoothScrollToPosition(0)
        }

        notificationManager.cancelGroupNotification(
            NOTIFICATION_GROUP_CHATS,
            message.baseNotificationId,
            message.summaryNotificationId,
        )

        service.api.markChatAsRead(chatID, message.messageId!!)
            .enqueue(service.handleApiResponse { })
    }

    /**
     * Load chats on initial view and on scroll
     * Marks Chat as read
     */
    private fun loadChats() {
        if (!loading && !lastPageReached) {
            setLoading(true)
            service.api.getMessagesForChat(chatID, PAGE_SIZE, nextPageToLoad).enqueue(
                service.handleApiResponse<ChatHistory>(
                    successCallback = {
                        when (it.code()) {
                            200 -> handleChatHistory(it.body()!!)
                            204 -> {
                                lastPageReached = true
                                noContentScreen.visibility = View.VISIBLE
                            }
                        }
                    },
                    alwaysDoLast = {
                        setLoading(false)
                    }
                )
            )
        }
    }

    /**
     * Handle the chat history
     * Renders the new messages and shows the noContnetScreen if this is a new chat.
     */
    private fun handleChatHistory(chatHistory: ChatHistory) {
        noContentScreen.visibility = View.GONE
        recyclerView.post {
            this.messages.addAll(chatHistory.messages)
            if (this.messages.size < 2) {
                recyclerViewAdapter.notifyDataSetChanged()
            } else {
                recyclerViewAdapter.notifyItemRangeInserted(
                    this.messages.size - chatHistory.messages.size,
                    this.messages.size
                )
            }
        }

        nextPageToLoad++
        lastPageReached = chatHistory.last

        // Mark Messages as read
        if (chatHistory.messages.size > 0)
            this.service.api.markChatAsRead(chatID, chatHistory.messages[0].id)
                .enqueue(service.handleApiResponse())
    }

    /**
     * Send a chat message and adds the chat to the recyclerview
     */
    private fun sendChatMessage() {
        val message = textInputField.nullableText()
        if (message != null) {
            textSendButton.isClickable = false
            service.api.sendMessageToChat(chatID, ChatMessageSend(message))
                .enqueue(service.handleApiResponse<ChatMessage>(
                    successCallback = {
                        noContentScreen.visibility = View.GONE
                        recyclerView.post {
                            this.messages.add(0, it.body()!!)
                            if (this.messages.size < 2) {
                                recyclerViewAdapter.notifyDataSetChanged()
                            } else {
                                recyclerViewAdapter.notifyItemInserted(0)
                            }
                            recyclerView.smoothScrollToPosition(0)
                        }
                        textInputField.text = null
                    },
                    alwaysDoLast = {
                        textSendButton.isClickable = true
                    }
                ))
        }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageListener)
        super.onDestroy()
    }

    /**
     * Switches back to ChatOverview
     */
    private fun switchToChatOverview() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            return
        }
        finish()
        super.onBackPressed()
    }

    /**
     * Setup the RecyclerView for Chat Messages
     */
    private fun setupRecyclerView() {
        recyclerView = findViewById(R.id.chat_recyclerview_messages)
        recyclerViewAdapter =
            ChatMessageRecyclerAdapter(messages, this, chatID, service, noContentScreen)
        recyclerView.adapter = recyclerViewAdapter
        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        // Reverse layout, the latest chat message is in the first position of the array.
        layoutManager.reverseLayout = true
    }

    /**
     * Setup Chat Buttons and Validators for Chat Inputfield and Button
     */
    private fun setupChatButtons() {
        textSendButton.setOnClickListener {
            sendChatMessage()
        }
    }

    /**
     * Setup the validator for the chatfield
     */
    private fun setupValidator() {
        validator = InputValidator(this, this)
            .registerInputs(validateTillInputs = listOf(textInputField))
            .setValidationCallbacks(
                { textSendButton.isEnabled = true },
                { textSendButton.isEnabled = false },
            )
    }

    /**
     * Setup Scroll Listener
     */
    private fun setupScrollListener() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !loading) {
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisItemPos = layoutManager.findFirstVisibleItemPosition()
                    if (ITEM_THRESHOLD > totalItemCount - (firstVisItemPos + visibleItemCount)) {
                        loadChats()
                    }
                }
            }
        })
    }

    /**
     * Set the Chat Title in the Action Bar. Usually set to the name of the chat.
     */
    private fun setChatTitle(name: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = name
    }

    /**
     * Set state of ChatOverview Loaders
     */
    private fun setLoading(state: Boolean) {
        loading = state
        if (messages.size <= 0 || !state) progressBar.visibility =
            if (state) View.VISIBLE else View.GONE
    }
}