package com.tintok.ui.chat

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.tintok.R
import com.tintok.api.ChatsService
import com.tintok.api.model.chats.ChatMessage
import java.text.SimpleDateFormat
import java.time.ZonedDateTime
import java.util.*

/**
 * Adapter for the recyclerview in the [ChatActivity].
 * creates and binds [ChatMessageHolder] or [UserChatMessageHolder] to display messages in the recyclerview
 * implements user interactions with the ViewHolders
 */
class ChatMessageRecyclerAdapter(
    private var chatMessages: MutableList<ChatMessage>,
    private var ctx: Context,
    private var chatID: String,
    private var api: ChatsService,
    private var noContentScreen: TextView
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View

        return when (viewType) {
            R.id.chat_own_msg -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.chat_message_own, parent, false)
                UserChatMessageHolder(view)
            }
            R.id.chat_singlemsg -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.chat_message_partner, parent, false)
                ChatMessageHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown Chat Type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserChatMessageHolder -> {
                addOnLongClickToHolder(holder)
                holder.messageText.text = chatMessages[position].message
                holder.timestamp.text = getTimeFromCreatedAt(chatMessages[position].CreatedAt)

            }
            is ChatMessageHolder -> {
                addOnLongClickToHolder(holder)
                holder.messageText.text = chatMessages[position].message
                holder.timestamp.text = getTimeFromCreatedAt(chatMessages[position].CreatedAt)
            }
            else -> throw IllegalArgumentException("Unknown Chat Type: $holder")
        }
    }

    override fun getItemViewType(position: Int) = when {
        chatMessages[position].send -> R.id.chat_own_msg
        else -> R.id.chat_singlemsg
    }

    override fun getItemCount() = chatMessages.size

    private fun addOnLongClickToHolder(
        holder: RecyclerView.ViewHolder
    ) {
        holder.itemView.setOnLongClickListener {
            MaterialAlertDialogBuilder(ctx)
                .setTitle(R.string.chat_removal_title)
                .setMessage(ctx.getString(R.string.chat_msg_removal_msg))
                .setNegativeButton(R.string.cancel) { _, _ -> }
                .setPositiveButton(R.string.delete) { _, _ ->
                    api.api.removeMessage(chatID, chatMessages[holder.adapterPosition].id).enqueue(api.handleApiResponse(
                        mustContainSuccessCode = 204,
                        successCallback = {
                            chatMessages.removeAt(holder.adapterPosition)
                            notifyItemRemoved(holder.adapterPosition)
                            if (chatMessages.size <= 0) {
                                noContentScreen.visibility = View.VISIBLE
                            }
                        }
                    ))
                }.show()

            return@setOnLongClickListener true
        }
    }

    /**
     * Converts the date time to the string shown below the chats as a timestamp.
     */
    private fun getTimeFromCreatedAt(date: ZonedDateTime): String {
        return SimpleDateFormat(
            "EEEE HH:mm",
            Locale.getDefault()
        ).format(Date(date.toEpochSecond() * DateUtils.SECOND_IN_MILLIS))
    }

    /**
     * OwnMessageHolder represents the class to show messages sent by the own user.
     */
    class UserChatMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById(R.id.chat_own_msg_field)
        var timestamp: TextView = itemView.findViewById(R.id.chat_own_msg_timestamp)
    }

    /**
     * ChatMessageHolder represents a chat message sent by another user to the viewing user.
     */
    class ChatMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById(R.id.chat_single_message)
        var timestamp: TextView = itemView.findViewById(R.id.chat_msg_timestamp)
    }
}