package com.tintok.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager
import android.util.Log
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.CancellationTokenSource
import com.tintok.R
import com.tintok.api.model.Location
import com.tintok.util.showPermissionDeniedWarning
import com.tintok.util.showPermissionNeverAskAgainWarning
import com.tintok.util.showPermissionRationale
import permissions.dispatcher.*

/**
 * This class handles permission checking for you and displays dialogs/toasts when the user rejects permissions
 */
@RuntimePermissions
abstract class FragmentUsingPermissions : FragmentUsingSnackBar() {

    var locationDeniedCallback = {}

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(
            requestCode,
            grantResults
        ) // Delegate permission handling to generated function
    }

    /**
     * Perform an action requiring the last/cached user location.
     * Calls performActionWithCurrentLocation(...) when no location is found in the cache
     */
    @SuppressLint("MissingPermission")
    @NeedsPermission(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun locationAction(callbacks: LocationCallbacks) {
        callbacks.first()
        val locationTask =
            LocationServices.getFusedLocationProviderClient(requireActivity()).lastLocation
        locationTask.addOnSuccessListener { location ->
            if (location != null) {
                callbacks.success(Location(location.latitude, location.longitude))
            } else {
                Log.d(
                    "CACHED_LOCATION_TASK: ",
                    "location is null, calling performActionWithCurrentLocation(...)"
                )
                performActionWithCurrentLocation(callbacks)
            }
        }
        locationTask.addOnFailureListener {
            Log.d("CACHED_LOCATION_TASK_FAILURE", it.message.toString())
            showSnackBarLocationFailed(callbacks.retry)
            callbacks.error()
        }
    }

    @SuppressLint("MissingPermission")
    private fun performActionWithCurrentLocation(callbacks: LocationCallbacks) {
        val errorCallback = {
            showSnackBarLocationFailed(callbacks.retry)
            callbacks.error()
        }

        val locationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
            )
        ) {
            val locationTask = LocationServices.getFusedLocationProviderClient(requireActivity())
                .getCurrentLocation(
                    LocationRequest.PRIORITY_HIGH_ACCURACY,
                    CancellationTokenSource().token
                )

            locationTask.addOnSuccessListener { location ->
                if (location != null) {
                    callbacks.success(Location(location.latitude, location.longitude))
                } else {
                    Log.d("CURRENT_LOCATION_TASK: ", "location is null")
                    errorCallback()
                }
            }
            locationTask.addOnFailureListener {
                Log.d("CURRENT_LOCATION_TASK_FAILURE", it.message.toString())
                errorCallback()
            }
        } else {
            errorCallback()
        }
    }

    @OnShowRationale(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun onLocationPermissionRationale(request: PermissionRequest) =
        requireContext().showPermissionRationale(
            request,
            R.string.permission_rationale_title_location,
            R.string.permission_rationale_location,
            locationDeniedCallback,
        )

    @OnPermissionDenied(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun onLocationPermissionDenied() {
        locationDeniedCallback()
        requireContext().showPermissionDeniedWarning(R.string.permission_denied_location)
    }

    @OnNeverAskAgain(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun onLocationPermissionNeverAskAgain() {
        locationDeniedCallback()
        requireContext().showPermissionNeverAskAgainWarning(
            R.string.permission_needed_title_location,
            R.string.permission_needed_location
        )
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun mediaAction(callback: MediaCallback) = callback.success()

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun onMediaPermissionRationale(request: PermissionRequest) =
        requireContext().showPermissionRationale(
            request,
            R.string.permission_rationale_title_media,
            R.string.permission_rationale_media,
        )

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun onMediaPermissionDenied() =
        requireContext().showPermissionDeniedWarning(R.string.permission_denied_media)

    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun onMediaPermissionNeverAskAgain() =
        requireContext().showPermissionNeverAskAgainWarning(
            R.string.permission_needed_title_media,
            R.string.permission_needed_media
        )

    fun showSnackBarLocationFailed(retry: () -> Unit) =
        this.showSnackBar(R.string.location_failed, retry)
}

class LocationCallbacks(
    val first: () -> Unit = {},
    val success: (location: Location) -> Unit,
    val error: () -> Unit = {},
    val retry: () -> Unit = {},
)

class MediaCallback(
    val success: () -> Unit,
)
