package com.tintok.ui

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.tintok.R

/**
 * This class makes sure the snackBar is dismissed when the user leaves the fragment
 */
abstract class FragmentUsingSnackBar : Fragment() {
    var snackBar: Snackbar? = null
    var snackBarAnchorViewId = R.id.nav_view

    override fun onDestroyView() {
        snackBar?.dismiss()
        super.onDestroyView()
    }

    /**
     * Show a snackBar to user with retry action
     */
    fun showSnackBar(message: Int, retry: () -> Unit) {
        view?.let {
            snackBar = Snackbar.make(
                it,
                message,
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(R.string.retry) {
                    if (isResumed) retry()
                    else snackBar?.dismiss()
                }
                .setAnchorView(snackBarAnchorViewId)
            snackBar?.show()
        }
    }
}