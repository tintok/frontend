package com.tintok.ui

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import com.tintok.R
import com.tintok.util.showPermissionDeniedWarning
import com.tintok.util.showPermissionNeverAskAgainWarning
import com.tintok.util.showPermissionRationale
import permissions.dispatcher.*

/**
 * This class handles permission checking for you and displays dialogs/toasts when the user rejects permissions
 */
@RuntimePermissions
abstract class ActivityUsingPermissions : AppCompatActivity() {

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults) // Delegate permission handling to generated function
    }

    /**
     * Perform an action requiring camera or storage access
     */
    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun mediaAction(callback: MediaCallback) = callback.success()

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun onMediaPermissionRationale(request: PermissionRequest) =
        this.showPermissionRationale(
            request,
            R.string.permission_rationale_title_media,
            R.string.permission_rationale_media
        )

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun onMediaPermissionDenied() =
        this.showPermissionDeniedWarning(R.string.permission_denied_media)

    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    fun onMediaPermissionNeverAskAgain() =
        this.showPermissionNeverAskAgainWarning(
            R.string.permission_needed_title_media,
            R.string.permission_needed_media
        )

}