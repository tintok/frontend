package com.tintok.ui.login

import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import androidx.constraintlayout.widget.Group
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.mobsandgeeks.saripaar.annotation.*
import com.tintok.*
import com.tintok.api.AuthService
import com.tintok.api.FcmService
import com.tintok.api.model.auth.LoginResponse
import com.tintok.api.model.auth.SignIn
import com.tintok.api.model.auth.Token
import com.tintok.api.model.fcm.FcmToken
import com.tintok.util.InputValidator
import com.tintok.util.hideKeyboard
import com.tintok.util.setKeyboardDoneAction
import com.tintok.util.showToast

/**
 * LoginFragment is used to show a login mask to the user if he is not logged in or his token has expired.
 * LoginFragment forwards the user to the MainActivity if the login was successful. The user can switch to the RegisterFragment to create a new account.
 */
class LoginFragment : Fragment() {


    @Email(messageResId = R.string.input_error_email)
    @Order(0)
    private lateinit var inputEmail: TextInputEditText

    @NotEmpty(trim = true, messageResId = R.string.input_error_password_empty)
    @Order(1)
    private lateinit var inputPassword: TextInputEditText

    // Views
    private lateinit var groupInputs: Group
    private lateinit var btnLogin: Button
    private lateinit var btnRegister: Button

    // Service
    private lateinit var service: AuthService
    private lateinit var fcmService: FcmService
    private lateinit var validator: InputValidator

    // Spinner
    private lateinit var spinner: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        bundle: Bundle?
    ): View {
        service = AuthService(requireContext())
        fcmService = FcmService(requireContext())
        val root = inflater.inflate(R.layout.fragment_login, container, false)
        groupInputs = root.findViewById(R.id.login_group_inputs)
        inputEmail = root.findViewById(R.id.login_input_email)
        inputPassword = root.findViewById(R.id.login_input_password)
        spinner = root.findViewById(R.id.login_loader)
        btnLogin = root.findViewById(R.id.login_btn_login)
        btnRegister = root.findViewById(R.id.login_btn_register)

        if (requireContext().hasValidSession()) {
            switchToMainActivity()
            return root
        } else if (requireContext().isTokenRefreshable())
            refreshToken(requireContext().getSessionToken())
        return root
    }

    /**
     * Setup input validation and button actions
     */
    private fun setupValidationAndButtons() {
        validator = InputValidator(this, requireContext())
            .registerInputs(listOf(inputEmail), listOf(inputPassword))
            .setValidationCallbacks(
                { btnLogin.isEnabled = true },
                { btnLogin.isEnabled = false },
            )
        btnRegister.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.loginContainer, RegisterFragment())
                .addToBackStack(null).commit()
        }

        setKeyboardDoneAction(inputPassword) {
            if (btnLogin.isEnabled) {
                btnLogin.performClick()
            }
        }
        btnLogin.setOnClickListener {
            hideKeyboard(requireContext(), it)
            handleUserLogin()
        }
    }


    /**
     * Send user login credentials to backend
     * and save session token on successful login
     */
    private fun handleUserLogin() {
        updateLoader(true)
        val login = SignIn(inputEmail.text.toString(), inputPassword.text.toString())

        service.api.postUserLogin(login).enqueue(
            service.handleApiResponse<LoginResponse>(
                mustContainSuccessCode = 200,
                successCallback = {
                    sendFcmTokenOnSuccessfulLogin()
                    onSuccessLoginResponse(it.body()!!)
                },
                errorCallback = {
                    showToast(requireContext(), R.string.login_error)
                    inputPassword.setText("")
                    btnLogin.isEnabled = false
                },
                failureCallback = {
                    Log.e("Login: HTTP_API_EXCEPTION", it.toString())
                },
                alwaysDoLast = { updateLoader(false) }
            ))
    }

    /**
     * Switch to the main activity
     */
    private fun switchToMainActivity() {
        val intent = Intent(requireActivity(), MainActivity::class.java)
        intent.flags = FLAG_ACTIVITY_CLEAR_TOP or FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        requireActivity().finish()
    }

    /**
     * Change visibility of spinner and the rest of the fragment
     */
    private fun updateLoader(loading: Boolean) {
        if (isResumed) {
            spinner.visibility = if (loading) View.VISIBLE else View.GONE
            groupInputs.visibility = if (loading) View.GONE else View.VISIBLE
        }
    }

    private fun onSuccessLoginResponse(loginResponse: LoginResponse) {
        if (isResumed) {
            requireContext().setSessionToken(loginResponse.prefix + " " + loginResponse.token)
            switchToMainActivity()
        }
    }

    /**
     * Refresh the session token
     */
    private fun refreshToken(token: String) {
        updateLoader(true)
        service.api.refreshUserToken(Token(token.split(" ")[1])).enqueue(
            service.handleApiResponse(
                mustContainSuccessCode = 200,
                successCallback = {
                    onSuccessLoginResponse(it.body()!!)
                },
                errorCallback = {
                    Log.e("Refresh", it.message() + " " + it.code())
                    showToast(requireContext(), R.string.refresh_error)
                },
                alwaysDoLast = { updateLoader(false) }
            )
        )
    }

    /**
     * Send the current FCM token to backend
     */
    private fun sendFcmTokenOnSuccessfulLogin() {
        getFcmToken {
            Log.d("FCM Token", it)
            fcmService.api.addFcmKeyToAccount(FcmToken(it)).enqueue(
                fcmService.handleApiResponse(
                    mustContainSuccessCode = 204
                )
            )
        }
    }

    override fun onResume() {
        setupValidationAndButtons()
        super.onResume()
    }
}