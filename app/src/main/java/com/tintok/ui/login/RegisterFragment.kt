package com.tintok.ui.login

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.mobsandgeeks.saripaar.annotation.*
import com.tintok.R
import com.tintok.api.AuthService
import com.tintok.api.model.auth.SignUp
import com.tintok.util.*

/**
 * RegisterFragment shows the Registration Form where a new account can be created.
 * If a new account was created, RegisterFragment switches back to LoginFragment to allow the user to log in.
 */
class RegisterFragment : Fragment() {

    @NotEmpty(messageResId = R.string.error_first_name, trim = true)
    @Length(max = 64, trim = true, messageResId = R.string.input_error_max_length_64)
    @Order(0)
    private lateinit var inputGivenName: TextInputEditText

    @NotEmpty(messageResId = R.string.error_last_name, trim = true)
    @Length(max = 64, trim = true, messageResId = R.string.input_error_max_length_64)
    @Order(1)
    private lateinit var inputFamilyName: TextInputEditText

    @Length(max = 64, messageResId = R.string.input_error_email_length)
    @Email(messageResId = R.string.input_error_email)
    @Order(2)
    private lateinit var inputEmail: TextInputEditText

    @Length(min = 6, max = 64, messageResId = R.string.input_error_password_length)
    @Password(messageResId = R.string.empty_string)
    @Order(3)
    private lateinit var inputPassword: TextInputEditText

    @ConfirmPassword(messageResId = R.string.input_error_password_confirm)
    @Order(4)
    private lateinit var inputPasswordConfirm: TextInputEditText

    // Views
    private lateinit var groupInputs: Group
    private lateinit var registerBtn: Button
    private lateinit var spinner: ProgressBar
    private lateinit var privacyPolicy : TextView

    // Service
    private lateinit var service: AuthService
    private lateinit var validator: InputValidator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View {
        service = AuthService(requireContext())
        val root = inflater.inflate(R.layout.fragment_register, container, false)
        inputGivenName = root.findViewById(R.id.register_input_given_name)
        inputFamilyName = root.findViewById(R.id.register_input_family_name)
        inputEmail = root.findViewById(R.id.register_input_email)
        inputPassword = root.findViewById(R.id.register_input_password)
        inputPasswordConfirm = root.findViewById(R.id.register_input_password_confirm)
        groupInputs = root.findViewById(R.id.register_group_inputs)
        spinner = root.findViewById(R.id.register_loader)
        registerBtn = root.findViewById(R.id.register_btn)
        privacyPolicy = root.findViewById(R.id.privacy_policy)
        privacyPolicy.movementMethod = LinkMovementMethod.getInstance()

        setupValidationAndButtons()

        return root
    }

    /**
     * Setup input validation and button actions
     */
    private fun setupValidationAndButtons() {
        validator = InputValidator(this, requireContext())
            .registerInputs(
                listOf(
                    inputGivenName,
                    inputFamilyName,
                    inputEmail,
                    inputPassword,
                ),
                listOf(inputPasswordConfirm)
            )
            .setValidationCallbacks(
                { registerBtn.isEnabled = true },
                { registerBtn.isEnabled = false },
            )
        setKeyboardDoneAction(inputPasswordConfirm) {
            if (registerBtn.isEnabled) {
                registerBtn.performClick()
            }
        }
        registerBtn.setOnClickListener {
            hideKeyboard(requireContext(), it)
            showAgb()
        }
    }

    /**
     * Shows the Agb dialog
     */
    private fun showAgb() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.agb_title)
            .setMessage(R.string.agb_content)
            .setNegativeButton(R.string.decline) { _, _ -> }
            .setPositiveButton(R.string.accept) { _, _ ->
                val email = inputEmail.nullableText()
                val givenName = inputGivenName.nullableText()
                val familyName = inputFamilyName.nullableText()

                if (email != null && givenName != null && familyName != null) {
                    postUserSignUp(email, givenName, familyName)
                } else {
                    showToast(requireContext(), R.string.error_required_fields_missing)
                }
            }
            .show().apply {
                findViewById<TextView>(android.R.id.message)?.movementMethod = LinkMovementMethod.getInstance()
            }
    }

    /**
     * Change visibility of spinner and the rest of the fragment
     */
    private fun updateLoader(loading: Boolean) {
        spinner.visibility = if (loading) View.VISIBLE else View.GONE
        groupInputs.visibility = if (loading) View.GONE else View.VISIBLE
    }

    /**
     * Send the sign up data to the server
     */
    private fun postUserSignUp(email: String, givenName: String, familyName: String) {
        updateLoader(true)
        val signUp = SignUp(email, inputPassword.text.toString(), givenName, familyName)

        service.api.postUserSignUp(signUp).enqueue(service.handleApiResponse<Unit>(
            mustContainSuccessCode = 201,
            successCallback = {
                switchToLoginFragment()
                showToast(requireContext(), R.string.signup_success)
            },
            alwaysDoLast = { updateLoader(false) }
        ))
    }

    private fun switchToLoginFragment() =
        parentFragmentManager.popBackStack()
}