package com.tintok.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tintok.R

/**
 * LoginActivity represents the Activity that is used to show the user a login screen or the register screen.
 * The Login Activity is started before the user is logged in or the token expired and the user is required to log in again.
 */
class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.loginContainer, LoginFragment())
                .commit()
        }
    }
}