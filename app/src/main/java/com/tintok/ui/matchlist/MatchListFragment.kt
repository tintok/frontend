package com.tintok.ui.matchlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.util.ViewPreloadSizeProvider
import com.tintok.R
import com.tintok.api.ChatsService
import com.tintok.api.model.chats.Match
import com.tintok.ui.FragmentUsingSnackBar
import com.tintok.util.GlideApp
import retrofit2.Response

/**
 * MatchListFragment is used to show all matches of an user.
 * The user can click on one of his matches to switch to the ChatActivity
 */
class MatchListFragment : FragmentUsingSnackBar() {
    private lateinit var service: ChatsService
    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: MatchListRecyclerAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var progressBar: ProgressBar
    private lateinit var noContentDescription: TextView
    private var matches: MutableList<Match> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        bundle: Bundle?
    ): View {
        val root = inflater.inflate(R.layout.fragment_match_list, container, false)
        progressBar = root.findViewById(R.id.chat_match_loader)
        noContentDescription = root.findViewById(R.id.chat_match_no_content)
        service = ChatsService(requireContext())
        loadMatches()
        setupRecyclerView(root)
        setupGlidePreloader()

        return root
    }

    /**
     * Setup the Recylerview for the Match List
     */
    private fun setupRecyclerView(root: View) {
        recyclerView = root.findViewById(R.id.matchlist_recyclerview)
        recyclerViewAdapter = MatchListRecyclerAdapter(matches, root.context)
        recyclerView.adapter = recyclerViewAdapter
        layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                layoutManager.orientation
            )
        )
    }

    /**
     * Load Matches
     */
    private fun loadMatches() {
        progressBar.visibility = View.VISIBLE
        service.api.getOwnMatches().enqueue(service.handleApiResponse<List<Match>>(
            successCallback = {
                handleSuccessCallback(it)
            },
            snackBarFragment = this,
            snackBarRetryAction = { loadMatches() },
            alwaysDoLast = { progressBar.visibility = View.GONE }
        ))
    }

    private fun handleSuccessCallback(it : Response<List<Match>>) {
        when(it.code()) {
            200 -> {
                noContentDescription.visibility = View.GONE
                val matches = it.body()!!
                this.matches.addAll(matches)
                recyclerView.post{
                    recyclerViewAdapter.notifyItemRangeInserted(this.matches.size - matches.size, this.matches.size)
                }
            }
            204 -> {
                noContentDescription.visibility = View.VISIBLE
            }
        }
    }

    private fun setupGlidePreloader() {
        val preloader: RecyclerViewPreloader<GlideUrl> =
            RecyclerViewPreloader(
                GlideApp.with(requireContext()),
                recyclerViewAdapter,
                ViewPreloadSizeProvider(),
                15
            )
        recyclerView.addOnScrollListener(preloader)
    }
}