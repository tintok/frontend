package com.tintok.ui.matchlist

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.model.GlideUrl
import com.tintok.R
import com.tintok.api.model.chats.Match
import com.tintok.ui.chat.CHAT_ID
import com.tintok.ui.chat.CHAT_NAME
import com.tintok.ui.chat.ChatActivity
import com.tintok.ui.chat.ChatListFragment
import com.tintok.util.GlideApp
import com.tintok.util.getUserImageUrl
import com.tintok.util.onlySingleSpaces
import java.util.*

/**
 * Adapter for the recyclerview in the [MatchListFragment].
 * creates and binds [MatchHolder] to display matches in the recyclerview
 * implements user interactions with the ViewHolders
 */
class MatchListRecyclerAdapter(
    private var matches: List<Match>,
    private var ctx: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), ListPreloader.PreloadModelProvider<GlideUrl> {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.chat_list_entry, parent, false)
        view.findViewById<TextView>(R.id.chat_unread)?.visibility =
            View.INVISIBLE // Disable Number Bubble
        return MatchHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MatchHolder -> {
                holder.messageText.text = matches[position].biography?.onlySingleSpaces()
                holder.username.text = matches[position].name
                holder.timestamp.text = matches[position].location

                GlideApp.with(this.ctx).asDrawable().load(getUserImageUrl(matches[position].id))
                    .applyDefaultProfileOptions()
                    .enableGlideCaching()
                    .into(holder.userIcon)
                // Switch to the chat if the user clicks on it
                holder.itemView.setOnClickListener {
                    val intent = Intent(it.context, ChatActivity::class.java)
                    intent.putExtra(CHAT_ID, matches[position].id)
                    intent.putExtra(CHAT_NAME, matches[position].name)
                    startActivity(it.context, intent, null)
                }
            }
            else -> throw java.lang.IllegalArgumentException("Invalid Type")
        }
    }

    override fun getItemCount() = matches.size

    /**
     * MatchHolder represents a match listed in the Recyclerview
     */
    class MatchHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById(R.id.chat_item_text)
        var username: TextView = itemView.findViewById(R.id.chat_name)
        var timestamp: TextView = itemView.findViewById(R.id.chat_time)
        var userIcon: ImageView = itemView.findViewById(R.id.chat_overview_icon)
    }

    override fun getPreloadItems(position: Int): MutableList<GlideUrl> {
        val list: MutableList<GlideUrl> = ArrayList()
        if (position < matches.size) {
            list.add(getUserImageUrl(matches[position].id))
        }
        return list
    }

    override fun getPreloadRequestBuilder(item: GlideUrl): RequestBuilder<*> =
        GlideApp.with(ctx).asDrawable().load(item)
}