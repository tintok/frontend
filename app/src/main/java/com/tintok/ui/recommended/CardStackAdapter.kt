package com.tintok.ui.recommended

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.tintok.R
import com.tintok.api.model.posts.Post
import com.tintok.api.model.posts.PostType
import com.tintok.ui.chat.ChatListFragment
import com.tintok.util.*

/**
 * Adapter for the recyclerview in the [RecommendedFragment].
 * creates and binds [ViewHolder] to display in the recyclerview
 * implements user interactions with the ViewHolders
 */
class CardStackAdapter(private val context: Context, private val posts: MutableList<Post>) :
    RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {

    private var players: MutableList<Player> = ArrayList()
    var lastSwipedPostId: String? = null

    override fun getItemViewType(position: Int) = posts[position].type.ordinal

    override fun onCreateViewHolder(parent: ViewGroup, postTypeOrdinal: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.recommended_card_view, parent, false), postTypeOrdinal)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post = posts[position]
        holder.textView.updateWithText(post.text, View.GONE, 25)

        when (holder.postTypeOrdinal) {
            PostType.IMAGE.ordinal -> {
                GlideApp.with(context).asDrawable().load(getPostImageUrl(post.id))
                    .applyDefaultImagePostsOptions(context)
                    .fitCenter()
                    .into(holder.imageView)
                holder.imageView.visibility = View.VISIBLE
            }
            PostType.VIDEO.ordinal -> {
                holder.videoPlayer.useController = false
                val player = getDefaultPlayerBuilder(context).build()
                players.add(player)
                holder.videoPlayer.player = player
                player.setMediaItem(MediaItem.fromUri(getVideoUri(post.id)))
                player.repeatMode = Player.REPEAT_MODE_ONE
                holder.videoPlayer.visibility = View.VISIBLE
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder.isVideoPost) {
            holder.videoPlayer.player?.pause()
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        if (holder.isVideoPost) {
            holder.videoPlayer.player?.let {
                players.remove(it)
                it.release()
            }
        }
    }

    override fun getItemCount() = posts.size

    fun pauseVideos() = players.forEach(Player::pause)

    fun releasePlayers() {
        players.forEach(Player::release)
        players.clear()
    }

    inner class ViewHolder(view: View, val postTypeOrdinal: Int) : RecyclerView.ViewHolder(view) {
        val textView: TextView = when (postTypeOrdinal) {
            PostType.TEXT.ordinal -> view.findViewById(R.id.recommended_post_text_center)
            else -> view.findViewById(R.id.recommended_post_text_bottom)
        }
        val imageView: ImageView = view.findViewById(R.id.recommended_post_image)
        val videoPlayer: StyledPlayerView = view.findViewById(R.id.recommended_post_video)
        val isVideoPost = postTypeOrdinal == PostType.VIDEO.ordinal
    }
}