package com.tintok.ui.recommended

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.tintok.R
import com.tintok.api.PostsService
import com.tintok.api.model.posts.Post
import com.tintok.ui.FragmentUsingPermissions
import com.tintok.ui.LocationCallbacks
import com.tintok.ui.locationActionWithPermissionCheck
import com.yuyakaido.android.cardstackview.CardStackLayoutManager
import com.yuyakaido.android.cardstackview.CardStackListener
import com.yuyakaido.android.cardstackview.CardStackView
import com.yuyakaido.android.cardstackview.Direction
import okhttp3.ResponseBody
import org.apache.commons.collections4.list.SetUniqueList
import java.time.ZonedDateTime
import java.util.*
import kotlin.collections.ArrayList

private const val ITEM_THRESHOLD: Int = 5
private const val PAGE_SIZE: Int = 10
private const val EXCEPT_LIST_BUFFER: Int = 80 // Max. 100 is allowed by backend

/**
 * RecommendedFragment shows recommended posts to the user which he can review by swiping left or right.
 */
class RecommendedFragment : FragmentUsingPermissions(), CardStackListener {

    // Services
    private lateinit var postsService: PostsService
    private lateinit var currentTimestamp: ZonedDateTime

    // Card stack
    private lateinit var cardStackView: CardStackView
    private lateinit var cardStackManager: CardStackLayoutManager
    private lateinit var cardStackAdapter: CardStackAdapter

    // Make List entries unique and also synchronize List against Threading problems.
    private val posts: MutableList<Post> = Collections.synchronizedList(
        SetUniqueList.setUniqueList(mutableListOf())
    )

    // Other views
    private lateinit var progressBar: ProgressBar
    private lateinit var recommendedTextNoContent: TextView

    // API Pagination
    private var lastPageReached: Boolean = false

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        bundle: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_recommended, container, false)
        postsService = PostsService(requireContext())
        currentTimestamp = ZonedDateTime.now()
        cardStackView = root.findViewById(R.id.card_stack_view)
        progressBar = root.findViewById(R.id.recommended_progress_bar)
        recommendedTextNoContent = root.findViewById(R.id.recommended_text_no_content)

        setupCardStackView()
        return root
    }

    override fun onCardAppeared(view: View, position: Int) {
        Log.d(
            javaClass.name,
            "CardPos $position and stackManagerPos ${cardStackManager.topPosition}"
        )
        val playerView: StyledPlayerView = view.findViewById(R.id.recommended_post_video)
        playerView.player?.let {
            it.prepare()
            it.play()
        }
    }

    override fun onCardDisappeared(view: View, position: Int) {
        cardStackAdapter.lastSwipedPostId = posts[position].id
    }

    override fun onCardSwiped(direction: Direction) {
        Log.d(javaClass.name, "CardSwiped stackManagerPos ${cardStackManager.topPosition}")
        cardStackAdapter.lastSwipedPostId?.let {
            when (direction) {
                Direction.Left -> {
                    postsService.api.dislikePost(it).enqueue(
                        postsService.handleApiResponse<ResponseBody>(204)
                    )
                }
                Direction.Right -> {
                    postsService.api.likePost(it).enqueue(
                        postsService.handleApiResponse<ResponseBody>(204)
                    )
                }
                else -> Log.e(
                    javaClass.name,
                    "Card was swiped with direction $direction, although it should not be allowed!"
                )
            }
        }
        when {
            shouldShowNoContentText() -> recommendedTextNoContent.visibility = View.VISIBLE
            shouldLoadNewPosts() -> loadPosts()
        }
    }

    override fun onCardDragging(direction: Direction, ratio: Float) {}

    override fun onCardRewound() {}

    override fun onCardCanceled() {}

    override fun onPause() {
        cardStackAdapter.pauseVideos()
        super.onPause()
    }

    override fun onDestroyView() {
        cardStackAdapter.releasePlayers()
        super.onDestroyView()
    }

    /**
     * Load new posts and add them to card stack
     */
    private fun loadPosts() {
        if (!lastPageReached) {
            locationActionWithPermissionCheck(
                LocationCallbacks(
                    success = { location ->
                        postsService.api.getPostsRecommended(
                            latitude = location.latitude,
                            longitude = location.longitude,
                            PAGE_SIZE,
                            exceptListPostIds()
                        ).enqueue(
                            postsService.handleApiResponse<List<Post>>(
                                successCallback = {
                                    Log.d(javaClass.name, it.toString())
                                    when (it.code()) {
                                        200 -> paginate(it.body()!!)
                                        204 -> {
                                            lastPageReached = true
                                            Log.d(
                                                javaClass.name,
                                                "CardStack top position ${cardStackManager.topPosition}"
                                            )
                                            Log.d(javaClass.name, "Posts size ${posts.size}")
                                            if (isResumed && shouldShowNoContentText()) {
                                                recommendedTextNoContent.visibility = View.VISIBLE
                                            }
                                        }
                                    }
                                },
                                snackBarFragment = this,
                                snackBarRetryAction = { loadPosts() },
                                alwaysDoLast = { updateLoader(false) }
                            )
                        )
                    },
                    error = { updateLoader(false) },
                    retry = {
                        updateLoader(true)
                        loadPosts()
                    },
                )
            )
        }
    }

    /**
     * Get the id's of remaining posts which have not been shown to the user
     */
    private fun exceptListPostIds(): Set<String> {
        var indexStart = cardStackManager.topPosition - EXCEPT_LIST_BUFFER
        if (indexStart < 0) indexStart = 0

        return when (indexStart) {
            in 0 until posts.size ->
                posts.slice(IntRange(indexStart, posts.size - 1)).map(Post::id).toSet()
            else -> emptySet()
        }
    }

    private fun shouldLoadNewPosts() = cardStackManager.topPosition >= posts.size - ITEM_THRESHOLD
    private fun isNoCardVisible() = cardStackManager.topPosition >= posts.size
    private fun shouldShowNoContentText() = isNoCardVisible() && lastPageReached

    private fun updateLoader(loading: Boolean) {
        if (isVisible) {
            progressBar.visibility = if (loading) View.VISIBLE else View.GONE
        }
    }

    /**
     * Setup the view for card swiping and load initial posts
     */
    private fun setupCardStackView() {
        cardStackManager = CardStackLayoutManager(requireContext(), this)
        cardStackAdapter = CardStackAdapter(requireContext(), posts)
        cardStackView.layoutManager = cardStackManager
        cardStackView.adapter = cardStackAdapter
        cardStackManager.setCanScrollVertical(false)

        locationDeniedCallback = {
            Log.d(javaClass.name, "Location denied callback!")
            updateLoader(false)
            showSnackBarLocationFailed { loadPosts() }
        }
        loadPosts()
    }

    /**
     * Add new cards to the stack and dispatch updates to the newly added cards
     */
    private fun paginate(postsToAdd: List<Post>) {
        val previousDataSetWasEmpty = posts.isEmpty()
        posts.addAll(postsToAdd)
        cardStackView.post {
            cardStackAdapter.notifyItemRangeInserted(
                posts.size - postsToAdd.size,
                postsToAdd.size
            )
            if (previousDataSetWasEmpty) cardStackAdapter.notifyDataSetChanged()
            cardStackView.visibility = View.VISIBLE
            Log.d(javaClass.name, "Added to cardStack: ${postsToAdd.size}")
        }
    }

}

