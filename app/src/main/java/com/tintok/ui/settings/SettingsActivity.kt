package com.tintok.ui.settings

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.messaging.FirebaseMessaging
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword
import com.mobsandgeeks.saripaar.annotation.Length
import com.mobsandgeeks.saripaar.annotation.Order
import com.mobsandgeeks.saripaar.annotation.Password
import com.tintok.R
import com.tintok.api.UsersService
import com.tintok.api.model.users.UpdatePassword
import com.tintok.clearUserData
import com.tintok.ui.login.LoginActivity
import com.tintok.util.InputValidator
import com.tintok.util.hideKeyboard
import com.tintok.util.setKeyboardDoneAction
import com.tintok.util.showToast

/**
 * SettingsActivity represents possible settings for the user. It is started by clicking on the Cog Icon on the ProfileFragment.
 */
class SettingsActivity : AppCompatActivity() {

    //Input fields
    @Length(min = 6, max = 64, messageResId = R.string.input_error_password_length)
    @Order(0)
    private lateinit var inputOldPassword: TextInputEditText

    @Length(min = 6, max = 64, messageResId = R.string.input_error_password_length)
    @Password(messageResId = R.string.empty_string)
    @Order(1)
    private lateinit var inputNewPassword: TextInputEditText

    @ConfirmPassword(messageResId = R.string.input_error_password_confirm)
    @Order(2)
    private lateinit var inputPasswordConfirm: TextInputEditText

    //Buttons
    private lateinit var btnChangePassword: Button
    private lateinit var btnLogout: Button
    private lateinit var btnDeleteAccount: Button

    private lateinit var service: UsersService
    private lateinit var validator: InputValidator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        service = UsersService(this)
        setContentView(R.layout.activity_settings)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        inputOldPassword = findViewById(R.id.settings_old_password_input)
        inputNewPassword = findViewById(R.id.settings_new_password_input)
        inputPasswordConfirm = findViewById(R.id.settings_confirm_password_input)

        btnChangePassword = findViewById(R.id.btn_change_password)
        btnLogout = findViewById(R.id.btn_logout)
        btnDeleteAccount = findViewById(R.id.btn_delete_acc)

        setupValidator()
        setupButtons()
    }

    /**
     * Setup the input validation
     */
    private fun setupValidator() {
        validator = InputValidator(this, this)
            .registerInputs(
                listOf(inputOldPassword, inputNewPassword),
                listOf(inputPasswordConfirm)
            ).setValidationCallbacks(
                { btnChangePassword.isEnabled = true },
                { btnChangePassword.isEnabled = false }
            )

        setKeyboardDoneAction(inputPasswordConfirm) {
            if (btnChangePassword.isEnabled) {
                btnChangePassword.performClick()
            }
        }
    }

    /**
     * Setup button actions
     */
    private fun setupButtons() {
        btnChangePassword.setOnClickListener {
            hideKeyboard(this, it)
            updatePassword()
        }

        btnLogout.setOnClickListener {
            hideKeyboard(this, it)
            logout()
        }

        btnDeleteAccount.setOnClickListener {
            hideKeyboard(this, it)
            MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.delete_account))
                .setMessage(getString(R.string.delete_account_message))
                .setNegativeButton(R.string.cancel) { _, _ -> }
                .setPositiveButton(getString(R.string.confirm)) { _, _ ->
                    deleteAccount()
                }
                .show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }


    /**
     * Sends an update request for the password to the server
     */
    private fun updatePassword() {
        service.api.putUpdatePassword(
            UpdatePassword(inputOldPassword.text.toString(), inputNewPassword.text.toString())
        ).enqueue(
            service.handleApiResponse(
                mustContainSuccessCode = 204,
                successCallback = {
                    validator.pauseValidationForCallback {
                        inputOldPassword.text = null
                        inputNewPassword.text = null
                        inputPasswordConfirm.text = null
                    }
                    showToast(this, getString(R.string.password_update_message))
                }
            ))
    }

    /**
     * Deletes the Fcm token and clears the app storage
     * if everything is successful return the user to the login screen
     */
    private fun logout() {
        FirebaseMessaging.getInstance().deleteToken().addOnSuccessListener {
            clearUserData()
            val notificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancelAll()
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags =
                Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
    }

    /**
     * Sends a request to delete the account to the server
     * and logs the user out successful
     */
    private fun deleteAccount() {
        service.api.deleteProfile().enqueue(
            service.handleApiResponse(
                mustContainSuccessCode = 204,
                successCallback = {
                    logout()
                }
            )
        )
    }


}