package com.tintok.api.model.chats

import androidx.recyclerview.widget.SortedList
import com.google.gson.annotations.SerializedName
import java.time.ZonedDateTime

/**
 * The body of a retrieve chat conversation request
 */
data class ChatHistory(
    @SerializedName("content")
    val messages: ArrayList<ChatMessage>,
    @SerializedName("last")
    val last: Boolean,
    @SerializedName("totalPages")
    val totalPages: Int,
    @SerializedName("totalElements")
    val totalElements: Int,
    // TODO: Add sort here maybe...
    @SerializedName("number")
    val number: Int,
    @SerializedName("first")
    val first: Boolean,
    @SerializedName("numberOfElements")
    val numberOfElements: Int,
    @SerializedName("size")
    val size: Int,
    @SerializedName("empty")
    val empty: Boolean
)

/**
 * Representation of a chat message
 */
data class ChatMessage(
    @SerializedName("id")
    val id: String,
    @SerializedName("created")
    val CreatedAt: ZonedDateTime,
    @SerializedName("message")
    val message: String,
    @SerializedName("send")
    val send: Boolean
)

/**
 * The body of a get all my chats response
 */
data class ChatList(
    @SerializedName("content")
    val messages: ArrayList<ChatConversation>,
    @SerializedName("last")
    val last: Boolean,
    @SerializedName("totalPages")
    val totalPages: Int,
    @SerializedName("totalElements")
    val totalElements: Int,
    @SerializedName("number")
    val number: Int,
    @SerializedName("first")
    val first: Boolean,
    @SerializedName("numberOfElements")
    val numberOfElements: Int,
    @SerializedName("size")
    val size: Int,
    @SerializedName("empty")
    val empty: Boolean
)

/**
 * Representation of a conversation
 */
data class ChatConversation(
    @SerializedName("partner")
    val chat_partner: ChatMember,
    @SerializedName("last")
    val last_message: ZonedDateTime,
    @SerializedName("unread")
    var unread: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("send")
    val send: Boolean
)

/**
 * ChatMember represents an user in a conversation
 */
data class ChatMember(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)

/**
 * ChatMessageSend models the API call to send a chat message to a new user.
 */
data class ChatMessageSend(
    @SerializedName("message")
    val message: String
)

/**
 * Match represents a Match which can be used to start a new chat.
 */
data class Match(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("biography")
    val biography: String?,
    @SerializedName("location")
    val location: String?
)
