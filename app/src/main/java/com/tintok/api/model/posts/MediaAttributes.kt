package com.tintok.api.model.posts


import com.google.gson.annotations.SerializedName
import com.tintok.api.model.Location

/**
 * MediaAttributes defines additional information for Video or Picture Posts on TinTok
 * This class is used for sending additional information for image and video posts, for handling TextPosts etc. please use the [TextPost] class
 */
data class MediaAttributes(

    @SerializedName("location")
    val location: Location,

    @SerializedName("tags")
    val tags: List<String>,

    @SerializedName("text")
    val text: String?

)