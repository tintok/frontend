package com.tintok.api.model.users

import com.google.gson.annotations.SerializedName

/**
 * GetUserMe represents the Response of the own User from the API
 */
data class GetUserMe(

    @SerializedName("id")
    val id: String,

    @SerializedName("username")
    val username: String,

    @SerializedName("given_name")
    val givenName: String,

    @SerializedName("family_name")
    val familyName: String,

    @SerializedName("location")
    val location: String?,

    @SerializedName("biography")
    val biography: String?

)