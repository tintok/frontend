package com.tintok.api.model.users


import com.google.gson.annotations.SerializedName

/**
 * PutUserMe defines the profile values sent by the User to the API when updating his profile.
 */
data class PutUserMe(

    @SerializedName("given_name")
    val givenName: String,

    @SerializedName("family_name")
    val familyName: String,

    @SerializedName("location")
    val location: String?,

    @SerializedName("biography")
    val biography: String?

)