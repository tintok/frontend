package com.tintok.api.model.posts

import com.google.gson.annotations.SerializedName


/**
 * The body of a home feed response
 */
data class FeedPostPage(

    @SerializedName("content")
    val post: List<Post>,

    @SerializedName("empty")
    val empty: Boolean,

    @SerializedName("first")
    val first: Boolean,

    @SerializedName("last")
    val last: Boolean,

    @SerializedName("number")
    val number: Int,

    @SerializedName("numberOfElements")
    val numberOfElements: Int,

    @SerializedName("size")
    val size: Int,

    @SerializedName("totalElements")
    val totalElements: Int,

    @SerializedName("totalPages")
    val totalPages: Int

)