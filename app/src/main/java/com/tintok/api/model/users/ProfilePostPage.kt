package com.tintok.api.model.users

import com.google.gson.annotations.SerializedName
import com.tintok.api.model.posts.Post

/**
 * ProfilePostPage defines the posts by the user itself which are then rendered on the Profile Page
 */
class ProfilePostPage(

    @SerializedName("content")
    val post: List<Post>,

    @SerializedName("empty")
    val empty: Boolean,

    @SerializedName("first")
    val first: Boolean,

    @SerializedName("last")
    val last: Boolean,

    @SerializedName("number")
    val number: Int,

    @SerializedName("numberOfElements")
    val numberOfElements: Int,

    @SerializedName("size")
    val size: Int,

    @SerializedName("totalElements")
    val totalElements: Int,

    @SerializedName("totalPages")
    val totalPages: Int

)