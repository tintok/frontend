package com.tintok.api.model


import com.google.gson.annotations.SerializedName

/**
 * Location represents the Location of the user in latitude/longitude form
 */
data class Location(

    @SerializedName("latitude")
    val latitude: Number,

    @SerializedName("longitude")
    val longitude: Number

)