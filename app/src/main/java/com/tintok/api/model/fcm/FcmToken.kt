package com.tintok.api.model.fcm

import com.google.gson.annotations.SerializedName

/**
 * FCMToken represents a notification token sent by the User to the backend to enable Push Notifications
 */
data class FcmToken(
    @SerializedName("token")
    val token: String

)