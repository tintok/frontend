package com.tintok.api.model.auth

import com.google.gson.annotations.SerializedName

/**
 * The body of a sign in request
 */
data class SignIn(

    @SerializedName("username")
    val username: String,

    @SerializedName("password")
    val password: String

)

/**
 * The body of a sign in response
 */
data class LoginResponse(

    @SerializedName("token")
    val token: String,

    @SerializedName("prefix")
    val prefix: String
)

/**
 * The body of a sign up request
 */
data class SignUp(
    @SerializedName("username")
    val username: String,

    @SerializedName("password")
    val password: String,

    @SerializedName("given_name")
    val givenName: String,

    @SerializedName("family_name")
    val familyName: String,
    @SerializedName("location")
    val location: String? = null,
    @SerializedName("biography")
    val biography: String? = null
)

/**
 * Representation of a user token
 */
data class Token(
    @SerializedName("token")
    val token : String
)