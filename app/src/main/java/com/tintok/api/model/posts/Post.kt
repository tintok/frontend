package com.tintok.api.model.posts

import com.google.gson.annotations.SerializedName
import com.tintok.api.model.Location
import java.time.ZonedDateTime
import java.util.*

/**
 * Representation of a post in a response and in the app
 */
data class Post(

    @SerializedName("author")
    val author: Author?,

    @SerializedName("created")
    val created: ZonedDateTime,

    @SerializedName("id")
    val id: String,

    @SerializedName("location")
    val location: Location,

    @SerializedName("tags")
    val tags: SortedSet<String>,

    @SerializedName("text")
    val text: String?,

    @SerializedName("type")
    val type: PostType,

    @SerializedName("updated")
    val updated: ZonedDateTime,

    @SerializedName("likes")
    var likes: Long?,

    @SerializedName("dislikes")
    var dislikes: Long?,

    @SerializedName("status")
    var status: PostStatus?
)

/**
 * PostType represents the available types for posts on TinTok
 */
enum class PostType {
    TEXT, VIDEO, IMAGE
}

/**
 * PostStatus represents the available status of the post for the user.
 */
enum class PostStatus {
    LIKED, DISLIKED, NEUTRAL
}
