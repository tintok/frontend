package com.tintok.api.model.posts

import com.google.gson.annotations.SerializedName

/**
 * Representation of a post author
 */
data class Author(

    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String

)