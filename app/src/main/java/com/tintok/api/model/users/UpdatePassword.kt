package com.tintok.api.model.users

import com.google.gson.annotations.SerializedName

/**
 * UpdatePassword represents a password change request to the API
 */
data class UpdatePassword(

    @SerializedName("old_password")
    val oldPassword: String,

    @SerializedName("new_password")
    val newPassword: String

)