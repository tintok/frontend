package com.tintok.api.model


import com.google.gson.annotations.SerializedName

/**
 * ApiError defines an error returned by the API (e.g. 400 - Invalid Location)
 */
data class ApiError(@SerializedName("message") val message: String)