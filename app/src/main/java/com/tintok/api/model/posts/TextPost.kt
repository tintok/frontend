package com.tintok.api.model.posts


import com.google.gson.annotations.SerializedName
import com.tintok.api.model.Location

/**
 * Representation of a TextPost in a request.
 * This is used to handle TextPosts from/to the backend, to send data within a image or video post, please use the [MediaAttributes] class.
 */
data class TextPost(

    @SerializedName("location")
    val location: Location,

    @SerializedName("tags")
    val tags: List<String>,

    @SerializedName("text")
    val text: String

)