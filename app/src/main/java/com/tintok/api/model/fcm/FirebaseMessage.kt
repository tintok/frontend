package com.tintok.api.model.fcm

import com.tintok.util.userIdToInt
import java.io.Serializable

/**
 * FirebaseMessage defines a response from Firebase Messaging which is then shown as a notification
 */
class FirebaseMessage(map: Map<String, String>) : Serializable {
    val id: String by map
    val name: String by map
    val message: String? by map
    val messageId : String? by map
    val type: String by map

    val summaryNotificationId: Int = MessageType.valueOf(type).ordinal
    val baseNotificationId: Int = baseNotificationId(id, summaryNotificationId)
}

/**
 * MessageTypes are all supported notification types from TinTok.
 */
enum class MessageType {
    CHAT,
    MATCH,
}

fun baseNotificationId(
    userId: String,
    summaryNotificationId: Int
) = userIdToInt(userId) + summaryNotificationId