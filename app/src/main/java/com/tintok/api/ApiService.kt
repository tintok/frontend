package com.tintok.api

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.tintok.R
import com.tintok.api.interfaces.*
import com.tintok.api.model.ApiError
import com.tintok.getSessionToken
import com.tintok.ui.FragmentUsingSnackBar
import com.tintok.util.showInternetError
import com.tintok.util.showToast
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

const val BASE_URL = "https://tintok.goip.de/"
const val CACHE_SIZE = 1024 * 1024 * 1024 // 1 GB

/**
 * Service for handling api calls
 */
abstract class ApiService(private val context: Context) {

    abstract val api: Any

    private val zonedDateTimeToStringConverterFactory = object : Converter.Factory() {
        override fun stringConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) =
            // Retrofit expects null if the supplied type is not supported by the Converter
            if (type == ZonedDateTime::class.java) {
                Converter<ZonedDateTime, String> { zonedDateTime ->
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(zonedDateTime)
                }
            } else null
    }

    private val gson = GsonBuilder()
        .registerTypeAdapter(ZonedDateTime::class.java, JsonDeserializer { json: JsonElement, _, _ ->
            ZonedDateTime.parse(json.asJsonPrimitive.asString, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        })
        .create()

    /**
     * Handle the response of a HTTP API call
     * @param mustContainSuccessCode - If provided the response must contain this code to be treated successful
     * @param successCallback - Statement executed on successful response
     * @param errorCallback - Statement executed on error response
     * @param failureCallback - Statement executed when backend connection not available
     * @param snackBarFragment - Fragment to display the snackBar on failing response
     * @param snackBarRetryAction - Statement executed when user hits snackBar action button
     * @param alwaysDoLast - Statement always executed after success/error/failure response handling
     */
    fun <T : Any> handleApiResponse(
        mustContainSuccessCode: Int? = null,
        successCallback: (response: Response<T>) -> Unit = {},
        errorCallback: (response: Response<T>) -> Unit = { response ->
            Log.e("HTTP_API_RESPONSE", response.toString())
            var errorMessage: String = context.getString(R.string.internal_error)
            try {
                val error =
                    Gson().fromJson(response.errorBody()!!.charStream(), ApiError::class.java)
                if (error.message.isNotBlank()) errorMessage = error.message
            } catch (exception: Exception) {
                Log.e("HandleApiResponse", "Exception parsing JSON. $exception")
                Log.e("HandleApiResponse", "Error Body was ${response.errorBody()}")
            } finally {
                showToast(context, errorMessage)
            }
        },
        failureCallback: (t: Throwable) -> Unit = {
            Log.e("API_EXCEPTION", it.toString())
        },
        snackBarFragment: FragmentUsingSnackBar? = null,
        snackBarRetryAction: () -> Unit = {},
        alwaysDoLast: () -> Unit = {},
    ): Callback<T> {
        return object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (
                    (mustContainSuccessCode == null && response.isSuccessful) ||
                    (mustContainSuccessCode == response.code())
                ) {
                    successCallback(response)
                } else {
                    errorCallback(response)
                }
                alwaysDoLast()
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                failureCallback(t)
                if (snackBarFragment != null && snackBarFragment.isResumed) {
                    snackBarFragment.showSnackBar(R.string.connection_failed, snackBarRetryAction)
                } else {
                    showInternetError(context)
                }
                alwaysDoLast()
            }
        }
    }

    /**
     * Get the default retrofit HTTP client
     */
    fun <T> getRetrofit(service: Class<T>, okHttpClient: OkHttpClient = getDefaultClient(context)): T {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(zonedDateTimeToStringConverterFactory)
            .client(okHttpClient)
            .build()
            .create(service)
    }
}

/**
 * Creates the default OkHttpClient
 * with caching enabled
 * and the appropriate "Accept-Language", "Authorization" headers
 */
fun getDefaultClient(context: Context): OkHttpClient {
    val cache = Cache(context.cacheDir, CACHE_SIZE.toLong())

    return clientWithAcceptLanguage(
        OkHttpClient.Builder()
            .cache(cache)
            .addInterceptor { chain ->
                val request = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", context.getSessionToken())
                    .build()
                chain.proceed(request)
            }
    ).build()
}

/**
 *  Creates an OkHttpClient.Builder
 *  with the appropriate "Accept-Language" header
 */
fun clientWithAcceptLanguage(okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()): OkHttpClient.Builder =
    okHttpClientBuilder.addInterceptor { chain -> // Dynamically assign user language
        val userLanguage = Locale.getDefault().toLanguageTag()
        val request = chain.request()
            .newBuilder()
            .addHeader("Accept-Language", userLanguage)
            .build()
        chain.proceed(request)
    }

/**
 * Handle user authentication
 */
class AuthService(context: Context) : ApiService(context) {
    override val api = getRetrofit(AuthApi::class.java, clientWithAcceptLanguage().build())
}

/**
 * Handle user conversations
 */
class ChatsService(context: Context) : ApiService(context) {
    override val api = getRetrofit(ChatsApi::class.java)
}


/**
 * Handle receiving and creating posts
 */
class PostsService(context: Context) : ApiService(context) {
    override val api = getRetrofit(PostsApi::class.java)
}

/**
 * Service for receiving the tag list
 */
class TagsService(context: Context) : ApiService(context) {
    override val api = getRetrofit(TagsApi::class.java)
}

/**
 * Handle all user profile interactions.
 */
class UsersService(context: Context) : ApiService(context) {
    override val api = getRetrofit(UserMeApi::class.java)
}


/**
 * Handle all FCM related requests.
 */
class FcmService(context: Context) : ApiService(context) {
    override val api = getRetrofit(FcmApi::class.java)
}
