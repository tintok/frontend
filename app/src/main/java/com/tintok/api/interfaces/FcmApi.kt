package com.tintok.api.interfaces

import com.tintok.api.model.fcm.FcmToken
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Implementation of the OpenApi Firebase Messaging (FCM) definition
 */
interface FcmApi {

    @POST("notifications/fcm")
    fun addFcmKeyToAccount(@Body token: FcmToken): Call<ResponseBody>

}