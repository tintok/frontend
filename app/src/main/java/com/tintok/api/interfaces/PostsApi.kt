package com.tintok.api.interfaces

import com.tintok.api.model.posts.FeedPostPage
import com.tintok.api.model.posts.MediaAttributes
import com.tintok.api.model.posts.Post
import com.tintok.api.model.posts.TextPost
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.time.ZonedDateTime

/**
 * Implementation of the OpenApi PostAPI definition
 */
interface PostsApi {

    @GET("posts/recommended")
    fun getPostsRecommended(
        @Query("latitude") latitude: Number,
        @Query("longitude") longitude: Number,
        @Query("size") size: Int,
        @Query("except") except: Collection<String>,
    ): Call<List<Post>>

    @GET("posts/feed")
    fun getPostFeed(
        @Query("size") size: Int?,
        @Query("page") page: Int?,
        @Query("naf") notAfter: ZonedDateTime?,
        @Query("nbf") notBefore: ZonedDateTime? = null,
        @Query("sort") sortAttribute: String? = null
    ): Call<FeedPostPage>

    @GET("posts/{id}/image")
    fun getImageData(@Path("id") id: String): Call<ResponseBody>

    @GET("posts/{id}/video")
    fun getVideoData(@Path("id") id: String): Call<ResponseBody>

    @Multipart
    @POST("posts/me")
    fun postImagePost(
        @Part("image_post") mediaAttributes: MediaAttributes,
        @Part image: MultipartBody.Part
    ): Call<ResponseBody>

    @Multipart
    @POST("posts/me")
    fun postVideoPost(
        @Part("video_post") mediaAttributes: MediaAttributes,
        @Part video: MultipartBody.Part
    ): Call<ResponseBody>

    @Multipart
    @POST("posts/me")
    fun postTextPost(@Part("text_post") textPost: TextPost): Call<ResponseBody>

    @POST("posts/{id}/like")
    fun likePost(@Path("id") id: String): Call<ResponseBody>

    @POST("posts/{id}/dislike")
    fun dislikePost(@Path("id") id: String): Call<ResponseBody>

    @POST("posts/{id}/clear")
    fun clearPost(@Path("id") id: String): Call<ResponseBody>

    @DELETE("posts/me/{id}")
    fun deletePost(@Path("id") id: String): Call<ResponseBody>
}