package com.tintok.api.interfaces

import com.tintok.api.model.auth.LoginResponse
import com.tintok.api.model.auth.SignIn
import com.tintok.api.model.auth.SignUp
import com.tintok.api.model.auth.Token
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Implementation of the OpenApi Auth definition
 */
interface AuthApi {

    @POST("/auth/sign-in")
    fun postUserLogin(@Body signIn: SignIn): Call<LoginResponse>

    @POST("/auth/sign-up")
    fun postUserSignUp(@Body signUp: SignUp): Call<Unit>

    @POST("/auth/refresh")
    fun refreshUserToken(@Body token : Token): Call<LoginResponse>

}