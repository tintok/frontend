package com.tintok.api.interfaces

import com.tintok.api.model.chats.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Implementation of the OpenApi Chat definition
 */
interface ChatsApi {
    @GET("/chats/{id}")
    fun getMessagesForChat(
        @Path("id") chatId: String,
        @Query("size") size: Int?,
        @Query("page") page: Int?,
    ): Call<ChatHistory>

    @POST("/chats/{id}/messages")
    fun sendMessageToChat(
        @Path("id") chatId: String,
        @Body message: ChatMessageSend
    ): Call<ChatMessage>

    @DELETE("/chats/{id}")
    fun leaveChat(@Path("id") chatId: String): Call<ResponseBody>

    @DELETE("/chats/{id}/messages/{messageId}")
    fun removeMessage(@Path("id") chatId : String, @Path("messageId") messageId : String) : Call<ResponseBody>

    @GET("/chats")
    fun listChats(@Query("size") size: Int?, @Query("page") page: Int?): Call<ChatList>

    @PUT("/chats/{id}/messages/{messageId}")
    fun markChatAsRead(
        @Path("id") chatId: String,
        @Path("messageId") messageId: String
    ): Call<ResponseBody>

    @GET("/users/me/matches")
    fun getOwnMatches(): Call<List<Match>>
}