package com.tintok.api.interfaces

import retrofit2.Call
import retrofit2.http.GET

/**
 * Implementation of the OpenApi TagsAPI definition
 */
interface TagsApi {

    @GET("tags")
    fun getAvailableTags(): Call<List<String>>

}