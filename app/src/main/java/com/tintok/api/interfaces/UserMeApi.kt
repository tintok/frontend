package com.tintok.api.interfaces

import com.tintok.api.model.users.GetUserMe
import com.tintok.api.model.users.ProfilePostPage
import com.tintok.api.model.users.PutUserMe
import com.tintok.api.model.users.UpdatePassword
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Implementation of the OpenApi UserMe definition
 */
interface UserMeApi {

    @GET("users/me")
    fun getMe(): Call<GetUserMe>

    @PUT("users/me")
    fun putMe(@Body putUserMe: PutUserMe): Call<ResponseBody>

    @GET("users/{userId}/profile-picture")
    fun getOwnProfilePicture(@Path("userId") id: String): Call<ResponseBody>

    @Multipart
    @POST("users/me/profile-picture")
    fun postOwnProfilePicture(@Part picture: MultipartBody.Part): Call<ResponseBody>

    @DELETE("users/me/profile-picture")
    fun deleteOwnProfilePicture(): Call<ResponseBody>

    @GET("posts/me")
    fun getOwnPosts(@Query("page") page: Int, @Query("size") size: Int): Call<ProfilePostPage>

    @PUT("users/me/password")
    fun putUpdatePassword(@Body updatePassword: UpdatePassword): Call<ResponseBody>

    @DELETE("users/me")
    fun deleteProfile(): Call<ResponseBody>
}