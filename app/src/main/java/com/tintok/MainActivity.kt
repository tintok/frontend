package com.tintok

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tintok.ui.feed.FeedFragment
import com.tintok.ui.profile.ProfileFragment

/**
 * MainActivity represents the default activity where the user can switch between fragments for posts, chats and profiles etc.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemReselectedListener {
            when (it.itemId) {
                R.id.navigation_home -> {
                    val fragment: Fragment? =
                        supportFragmentManager.findFragmentById(R.id.nav_host_fragment)!!.childFragmentManager.primaryNavigationFragment
                    if (fragment is FeedFragment)
                        fragment.scrollToTop()
                }
                R.id.navigation_profile -> {
                    val fragment: Fragment? =
                        supportFragmentManager.findFragmentById(R.id.nav_host_fragment)!!.childFragmentManager.primaryNavigationFragment
                    if (fragment is ProfileFragment)
                        fragment.scrollToTop()
                }
            }
        }
        navView.setupWithNavController(
            findNavController(R.id.nav_host_fragment)
        )
    }

}